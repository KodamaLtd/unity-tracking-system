﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public struct NoiseResult
{
  public double range;

  public int actualCount;
  public double sum;
  public double sumOfSquares;

  // mean stats
  public double meanSum;
  public double meanSumOfSquares;

  // sd stats
  public double sdSum;
  public double sdSumOfSquares;

  // peak-to-peak stats
  public double min;
  public double max;

  public void Zap()
  {
    actualCount = 0;
    sum = 0;
    sumOfSquares = 0;
    meanSum = 0;
    meanSumOfSquares = 0;
    sdSum = 0;
    sdSumOfSquares = 0;
    min = 0;
    max = 0;
  }

  public void AddResult(double value)
  {
    if (double.IsNaN(value) || double.IsInfinity(value))
      return;

    ++actualCount;
    sum += value;
    sumOfSquares += value * value;

    double thisMean = Mean();
    meanSum += thisMean;
    meanSumOfSquares += thisMean * thisMean;

    double thisSD = StandardDeviationInPPM();
    sdSum += thisSD;
    sdSumOfSquares += thisSD * thisSD;

    if (actualCount == 1)
    {
      max = value;
      min = value;
    }
    else
    {
      if (value > max)
        max = value;

      if (value < min)
        min = value;
    }
  }

  public double Mean()
  {
    if (actualCount == 0)
      return(0);
    return (sum / actualCount);
  }

  public double MeanInPPM()
  {
    if (range == 0)
      return (0);

    return (Mean() * 1e6 / range);
  }

  public double StandardDeviation()
  {
    if (actualCount == 0)
      return (0);

    double mean = Mean();
    double standardDeviation = Math.Sqrt(sumOfSquares / actualCount - mean * mean);
    return (standardDeviation);
  }

  public double StandardDeviationInPPM()
  {
    if (actualCount == 0)
      return (0);
    return (StandardDeviation() * 1e6 / range);
  }

  public double MeanSD()
  {
    if (range == 0)
      return (0);

    return (sdSum / actualCount);
  }

  public double SDSD()
  {
    if (actualCount == 0)
      return (0);

    double mean = MeanSD();
    double standardDeviation = Math.Sqrt(sdSumOfSquares / actualCount - mean * mean);
    return (standardDeviation);
  }

  public double RSESD()
  {
    // return (SDSD() / StandardDeviationInPPM());
    if (actualCount <= 1)
      return (0);

    return (1 / Math.Sqrt(2 * (actualCount - 1)));
  }

  public double PeakToPeak()
  {
    return (max-min);
  }

  public override string ToString()
  {
    StringBuilder thisString = new StringBuilder();
    thisString.Append(range);
    thisString.Append(",");
    thisString.Append(actualCount);
    thisString.Append(",");
    // we don't want zero values on a log graph
    thisString.Append(string.Format("{0:0.000}", MeanInPPM()));
    thisString.Append(",");
    thisString.Append(string.Format("{0:0.000}", StandardDeviationInPPM()));
    return (thisString.ToString());
  }

  public string ToResultString(double? nominal,char delimiter=' ')
  {
    // "timestamp,internal temp (C),frequency (Hz),mean (ppm),noise (ppm),");
    StringBuilder thisString = new StringBuilder();

    if (nominal != 0)
    {
      double ppm = (Mean()-(double)nominal) * 1e6 / (double)nominal;
      thisString.Append(string.Format("{0:0.000}", ppm));
      thisString.Append(delimiter);
      ppm = StandardDeviation() * 1e6 / (double)nominal;
      thisString.Append(string.Format("{0:0.000}", ppm));
    }
    else
    {
      thisString.Append(string.Format("{0:0.000000000}", Mean()));
      thisString.Append(delimiter);
      thisString.Append(string.Format("{0:0.000000000}", StandardDeviation()));
    }

    return (thisString.ToString());
  }
}
