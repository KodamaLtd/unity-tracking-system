﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;
using System;
using System.IO;
using System.Threading;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Diagnostics;

// MathNet matrix and maths classes
using MathNet.Numerics;
// Matrix<>
using MathNet.Numerics.LinearAlgebra;
// DenseMatrix
using MathNet.Numerics.LinearAlgebra.Double;
// Random numbers
using MathNet.Numerics.Random;
// optimisation
using MathNet.Numerics.Optimization;

// MLAT gradient descent
using mlat;



public class Reading
// a single magnetometer reading - this is in magnetometer / Pololu board co-ordinates
// i.e. Z is Down->Up, X is Left->Right, Y is Front->Back
{
  public uint sampleCount = 0;
  public uint timeStamp = 0;
  public Vector3 vector;
  public Vector3 noise;
  public uint scaleFactor = 1;
  
}

public class TrackingSystem : MonoBehaviour
{
  SerialPort serial;
  int spBufferLineCount;
  bool uPIsProcessingCommands = false;
  bool hasSentSmoothingFactors = false;

    [Range(1.0f, 10.0f)]
    public float altitudeScaleFactor = 3.7f;

    enum misc { NO_OF_MAGNETOMETERS = 13, NO_OF_DATA_ITEMS_PER_READING = 9, MAX_FRAME_RATE = 30,
    // i.e. 4 x the max reading from 4G to 16G scaling
    MAX_SCALING_FACTOR = 4,
    SATURATION_LIMIT = 30000,
    COM_PORT = 37, COM_PORT_SPEED = 1000000, // COM_PORT_SPEED=921600,
    SENSOR_SEPARATION_IN_MM = 75, SERIAL_TIMEOUT_IN_MS = 5000,
    DATA_START_CHAR = 'D', NOISE_START_CHAR = 'N', ACK_CHAR='A', NOISE_HEADINGS_START_CHAR='H',
    DATA_DELIMITER_CHAR = ',', CR = 13, LF = 10,
    // max limits of calculated single magnetomer position vector and 
    MAX_POSITION_VECTOR_LENGTH = 150, MAX_OVERALL_POSITION_LENGTH = 200,
    READING_SCALING=10};

  enum positionAlgorithm
  {
    ORIGINAL, WEIGHTED_AVERAGE_DISTANCE, BASIC_TRILATERATION, SEOKSEONG_JEON_TRILATERATION
  };
  positionAlgorithm currentPositionAlgorithm = positionAlgorithm.ORIGINAL;

  enum magnetometerName { BL, TL, MI, BR, TR, LC, TC, RC, BC, BLI, TLI, TRI, BRI};
  // sensor X/Z positions
  protected Vector3[] magnetometerCoordinates = { new Vector3(-1, -1, 0), new Vector3(-1, 1, 0), new Vector3(0, 0, 0), new Vector3(1, -1, 0), new Vector3(1, 1, 0),
                                                  new Vector3(-1, 0, 0), new Vector3(0, 1, 0), new Vector3(1, 0, 0), new Vector3(0, -1, 0),
                                                  new Vector3(-0.5f, -0.5f, 0), new Vector3(-0.5f, 0.5f, 0), new Vector3(0.5f, 0.5f, 0), new Vector3(0.5f, -0.5f, 0)};

  static string[] magnetometerNames = Enum.GetNames(typeof(magnetometerName));

  static char[] commaSplit = { (char)misc.DATA_DELIMITER_CHAR };
  static string[] crNlSplit = { "\r\n" };

  // this represents the difference between the uP and app magnetometer order
  int[] magnetometerOrder = { 1, 2, 4, 5, 3 , 6, 7, 8, 9, 10, 11, 12, 13};

  public GameObject[] magnetometers;
  // noise objects to put on the end of reading 'stalk'
  public GameObject[] noiseObjects;

  SensorReadingsPacket sensorPacket;
  int SIZE_OF_PACKET = Marshal.SizeOf(typeof(SensorReadingsPacket));
  byte[] serialBuffer;
  int noOfMessages;
  protected DateTime lastUpdateTime;
  protected double readTimeInMS;

  // readings from all magnetometers
  // indexes into magnetometer reading array
  enum magnetometerPositions { BOTTOM_LEFT, TOP_LEFT, CENTRE, BOTTOM_RIGHT, TOP_RIGHT, LEFT_CENTRE, TOP_CENTRE, RIGHT_CENTRE, BOTTOM_CENTRE, BLI, TLI, TRI, DRI };
  Reading[] readings = new Reading[(int)misc.NO_OF_MAGNETOMETERS];

  public GameObject puck;

  // UI objects
  public Text positionText;
  public Text serialStats;
  public Text[] sensorData = new Text[(int)misc.NO_OF_MAGNETOMETERS];

  public Text uPNoiseHeadings;
  public Text uPNoiseStats;

  public Button clearNoiseCalcButton;
  public Button recalibrateButton;
  public Button reConnectButton;

  public Text emaPeriodText;
  public Slider emaSlider;
  public Slider cutoffSlider;
  public Text cutoffText;
  public Slider betaSlider;
  public Text betaText;

  public Dropdown statisticDropdown;
  public Dropdown positionAlgorithmDropdown;

  public Text statusBar;

  public Toggle singleSensorToggle;
  public Toggle showReadingsToggle;
  public Toggle showNoiseBubblesToggle;
  public Toggle showDistancesToggle;
  public Toggle showInfluencesToggle;
  public Toggle showPositionsToggle;
  public Toggle showPuckToggle;

  public Dropdown influenceStyle;
  public Slider noOfSensors;
  public Text noOfSensorsValue;

  public Slider triLatPowerSlider;
  public Text triLatPowerValue;
  public Text triLatErrorValue;

  bool singleSensor =false, showReadings=false, showNoiseBubbles=false, showDistances=false, showInfluences=false, showPositions = false, showPuck=true;
  double triLatPower;

  protected NoiseResult noise;
  // for calculated puck position
  protected LineRenderer puckVector;
  // for individual magnetometer readings
  protected LineRenderer[] readinglineRenderers=new LineRenderer[(int)misc.NO_OF_MAGNETOMETERS];
  protected LineRenderer[] positionlineRenderers = new LineRenderer[(int)misc.NO_OF_MAGNETOMETERS];
  protected LineRenderer[] distanceRenderers = new LineRenderer[(int)misc.NO_OF_MAGNETOMETERS];
  protected LineRenderer[] influenceRenderers = new LineRenderer[(int)misc.NO_OF_MAGNETOMETERS];
  protected NoiseResult[] sensorNoise=new NoiseResult[(int)misc.NO_OF_MAGNETOMETERS];

  protected bool hasStarted = false;

  // used to manipulate influence factors
  protected class DistanceAndInfluence
  {
    public int magnetometerNo;
    public float distance;
    // between zero and one
    public float influence;
  }

  protected DistanceAndInfluence[] distancesAndInfluences=new DistanceAndInfluence[(int)misc.NO_OF_MAGNETOMETERS];
  protected DistanceAndInfluence[] sortedDistancesAndInfluences = new DistanceAndInfluence[(int)misc.NO_OF_MAGNETOMETERS];

  int currentInfluenceOrder = 0;
  int currentNoOfInfluenceSensors=3;
  protected static double INFLUENCE_POWER = 3;

  // used to show which sensors are used in the calculation
  bool[] sensorsUsedInCalculation = new bool[(int)misc.NO_OF_MAGNETOMETERS];

  // instrumentation
  Stopwatch stopwatch = new Stopwatch();

  // saved puck position
  Vector3 lastPuckPositionInUnityAxes;

  void Awake()
  {
   // set the max frame rate because we want to get as many samples as we can for an acceptable frame rate 
    QualitySettings.vSyncCount = 0;
    Application.targetFrameRate = (int)misc.MAX_FRAME_RATE;
  }

  GameObject CreateGameObject(PrimitiveType thisPrimitive,string thisName,Vector3 thisPosition, int thisSize)
  {
    GameObject thisGameObject = GameObject.CreatePrimitive(thisPrimitive);
    thisGameObject.isStatic = true;
    thisGameObject.name = thisName;
    thisGameObject.transform.position = thisPosition;
    thisGameObject.transform.localScale = new Vector3(thisSize, thisSize, thisSize);
    return (thisGameObject);
  }

  LineRenderer AddLine(GameObject thisObject, Color thisColour,int thisWidth)
  {
    LineRenderer thisLine=thisObject.AddComponent<LineRenderer>();
    thisLine.material = new Material(Shader.Find("Sprites/Default"));
    thisLine.material.SetColor("_Color", thisColour);
    thisLine.widthMultiplier = thisWidth;
    thisLine.positionCount = 2;
    return (thisLine);
  }

  // Use this for initialization
  void Start()
  {
    // puck
    puck = CreateGameObject(PrimitiveType.Cube, "puck", Vector3.zero, 5);
    puckVector = AddLine(puck, Color.green, 1);

    // magnetometer sensor & noise bubble game objects
    magnetometers = new GameObject[(int)misc.NO_OF_MAGNETOMETERS];
    noiseObjects = new GameObject[magnetometers.Length];

    for (int idy=0;idy<(int)misc.NO_OF_MAGNETOMETERS;++idy)
    {
      Vector3 thisSensorPosition = magnetometerCoordinates[idy]* (float)misc.SENSOR_SEPARATION_IN_MM;
      // flip from sensor axes to Unity axes by swapping y and z axes
      GameObject thisMagnetometer = CreateGameObject(PrimitiveType.Sphere, "sensor " + magnetometerNames[idy],new Vector3(thisSensorPosition.x, thisSensorPosition.z, thisSensorPosition.y), 5);
      magnetometers[idy] = thisMagnetometer;

      // reading vectors (add to sensor)
      readinglineRenderers[idy] = AddLine(thisMagnetometer, Color.white, 1);

      // noise bubbles
      GameObject thisNoiseBubble = CreateGameObject(PrimitiveType.Sphere, "noise " + magnetometerNames[idy], Vector3.zero, 1); ;
      noiseObjects[idy] = thisNoiseBubble;

      // various illustation vectors
      GameObject thisDistanceVector = CreateGameObject(PrimitiveType.Sphere, "distance " + magnetometerNames[idy], Vector3.zero, 1); ;
      distanceRenderers[idy] = AddLine(thisDistanceVector, Color.yellow, 1);

      // make the influence vectors wider so they appear over the distance vectors
      GameObject thisInfluenceVector = CreateGameObject(PrimitiveType.Sphere, "influence " + magnetometerNames[idy], Vector3.zero, 3); ;
      influenceRenderers[idy] = AddLine(thisInfluenceVector, Color.gray, 3);
    }

    // UI events
    clearNoiseCalcButton.onClick.AddListener(clearNoiseCalcButtonOnClick);
    recalibrateButton.onClick.AddListener(recalibrateButtonOnClick);
    // reConnectButton.onClick.AddListener(reConnectButtonOnClick);
    emaSlider.onValueChanged.AddListener(emaSliderOnValueChange);
    cutoffSlider.onValueChanged.AddListener(cutoffSliderOnValueChange);
    betaSlider.onValueChanged.AddListener(betaSliderOnValueChange);
    statisticDropdown.onValueChanged.AddListener(statisticOnValueChange);
    positionAlgorithmDropdown.onValueChanged.AddListener(positionAlgorithmOnValueChange);

    singleSensorToggle.onValueChanged.AddListener(singleSensorToggleChange);
    showReadingsToggle.onValueChanged.AddListener(showReadingsToggleChange);
    showNoiseBubblesToggle.onValueChanged.AddListener(showNoiseBubblesToggleChange);
    showDistancesToggle.onValueChanged.AddListener(showDistancesToggleChange);
    showInfluencesToggle.onValueChanged.AddListener(showInfluencesToggleChange);
    showPositionsToggle.onValueChanged.AddListener(showPositionsToggleChange);
    showPuckToggle.onValueChanged.AddListener(showPuckToggleChange);

    influenceStyle.onValueChanged.AddListener(influenceStyleOnValueChange);
    noOfSensors.onValueChanged.AddListener(noOfSensorsOnValueChange);

    triLatPowerSlider.onValueChanged.AddListener(triLatPowerSliderOnValueChange);

    // now open the comms
    if (openSerialPort())
      hasStarted = true;
  }

  void clearNoiseCalcButtonOnClick()
  {
    // can be cleared when the puck has come to rest
    noise.Zap();
    for(int idx=0;idx<(int)misc.NO_OF_MAGNETOMETERS;++idx)
      sensorNoise[idx].Zap();

  }

  void recalibrateButtonOnClick()
  {
    // ask the uP to run recalibration
    if (serial != null && serial.IsOpen)
    {
      serial.WriteLine("R");
      serial.BaseStream.Flush();
    }
  }

  void reConnectButtonOnClick()
  {
    // re-open the serial port
    if (serial != null && serial.IsOpen)
      serial.Close();
    openSerialPort();
  }

  void emaSliderOnValueChange(float thisValue)
  {
    // send the uP the new EMA period
    if (serial != null && serial.IsOpen)
    {
      int thisPeriod = (int)thisValue;
      serial.WriteLine("E," + thisPeriod);
      serial.BaseStream.Flush();
      emaPeriodText.text = thisPeriod.ToString() + " Frame" + (thisPeriod == 1 ? "" : "s");
    }
  }

  void cutoffSliderOnValueChange(float thisValue)
  {
    // send the uP the new EMA period
    String thisValueString = thisValue.ToString("0.0");
    if (serial != null && serial.IsOpen)
    {
      serial.WriteLine("C," + thisValueString);
      serial.BaseStream.Flush();
    }
    cutoffText.text = thisValueString;
  }

  void betaSliderOnValueChange(float thisValue)
  {
    // send the uP the new EMA period
    String thisValueString = thisValue.ToString("0.00");
    if (serial != null && serial.IsOpen)
    {
      serial.WriteLine("B," + thisValueString);
      serial.BaseStream.Flush();
    }
    betaText.text = thisValueString;
  }

  void statisticOnValueChange(int thisValue)
  {
    // send the uP a new statistic choice
    // note that the combobox index matches the uP enum
    if (serial != null && serial.IsOpen)
    {
      serial.WriteLine("ST," + thisValue);
      serial.BaseStream.Flush();
    }
  }

  void positionAlgorithmOnValueChange(int thisValue)
  {
    currentPositionAlgorithm = (positionAlgorithm)thisValue;
    triLatErrorValue.text = string.Empty;
  }

  void singleSensorToggleChange(bool thisValue)
  {
    singleSensor = thisValue;
  }
  void showReadingsToggleChange(bool thisValue)
  {
    showReadings = thisValue;
  }

  void showNoiseBubblesToggleChange(bool thisValue)
  {
    showNoiseBubbles = thisValue;
  }

  void showDistancesToggleChange(bool thisValue)
  {
    showDistances = thisValue;
  }

  void showInfluencesToggleChange(bool thisValue)
  {
    showInfluences = thisValue;
  }

  void showPositionsToggleChange(bool thisValue)
  {
    showPositions = thisValue;
  }

  void showPuckToggleChange(bool thisValue)
  {
    showPuck = thisValue;
  }

  void influenceStyleOnValueChange(int thisValue)
  {
    currentInfluenceOrder = thisValue;
  }

  void noOfSensorsOnValueChange(float thisValue)
  {
    currentNoOfInfluenceSensors = (int)thisValue;
    noOfSensorsValue.text = currentNoOfInfluenceSensors.ToString();
  }

  void triLatPowerSliderOnValueChange(float thisValue)
  {
    triLatPower = Math.Round(thisValue,1);
    triLatPowerValue.text = triLatPower.ToString("0.0");
    triLatErrorValue.text = String.Empty;
  }

  void handshake()
  {
    // check that the uP is processing commands
    if (serial != null && serial.IsOpen)
    {
      serial.WriteLine("H");
      serial.BaseStream.Flush();
    }
  }

  void parseArduinoData(String thisData)
  {
    // ignore empty strings
    if (String.IsNullOrEmpty(thisData))
      return;

    // quickly ignore lines that are not data, i.e. don't have a comma in position 2
    if (thisData.Length < 2 || thisData[1] != (char)misc.DATA_DELIMITER_CHAR)
      return;

    string[] splitArray = thisData.Split(commaSplit); //Here we assing the splitted string to array by that char

    // ignore obviously unparsable strings
    // '+1' as the line should start with 'D,'
    if (splitArray.Length != (int)misc.NO_OF_MAGNETOMETERS * (int)misc.NO_OF_DATA_ITEMS_PER_READING + 1)
      return;

    for (int idy = 0; idy < (int)misc.NO_OF_MAGNETOMETERS; ++idy)
    {
      int dataIndex = idy * (int)misc.NO_OF_DATA_ITEMS_PER_READING + 1;
      Reading thisReading = new Reading();

      float x, y, z;

      float.TryParse(splitArray[dataIndex], out x);
      float.TryParse(splitArray[++dataIndex], out y);
      float.TryParse(splitArray[++dataIndex], out z);

      Vector3 thisMagnetometerReading = new Vector3(x, y, z);
      thisReading.vector = thisMagnetometerReading;

      // time stamp is in uP milliseconds
      uint sampleCount, timeStamp;
      uint.TryParse(splitArray[++dataIndex], out sampleCount);
      uint.TryParse(splitArray[++dataIndex], out timeStamp);

      thisReading.sampleCount = sampleCount;
      thisReading.timeStamp = timeStamp;

      float xNoise, yNoise, zNoise;

      float.TryParse(splitArray[++dataIndex], out xNoise);
      float.TryParse(splitArray[++dataIndex], out yNoise);
      float.TryParse(splitArray[++dataIndex], out zNoise);

      Vector3 thisNoise = new Vector3(xNoise, yNoise, zNoise);
      thisReading.noise = thisNoise;

      uint scaleFactor;
      uint.TryParse(splitArray[++dataIndex], out scaleFactor);
      thisReading.scaleFactor = scaleFactor;

      /* note that the order is slightly juggled from the PCB sensor order and the directions
         of the axes are different too */
      int magnetometerNo = magnetometerOrder[idy];
      readings[magnetometerNo - 1] = thisReading;
    }
  }

  void parseArduinoPacket(SensorReadingsPacket thisPacket)
  {
    for (int idy = 0; idy < (int)misc.NO_OF_MAGNETOMETERS; ++idy)
    {
      Reading thisReading = new Reading();
      SensorOutputData thisData=new SensorOutputData();
      // as the structure is a marshalled C struct the sensor data can't be an array, 
      switch(idy)
      {
        case 0:
          thisData = thisPacket.sensorData1;
          break;
        case 1:
          thisData = thisPacket.sensorData2;
          break;
        case 2:
          thisData = thisPacket.sensorData3;
          break;
        case 3:
          thisData = thisPacket.sensorData4;
          break;
        case 4:
          thisData = thisPacket.sensorData5;
          break;

        case 5:
          thisData = thisPacket.sensorData6;
          break;
        case 6:
          thisData = thisPacket.sensorData7;
          break;
        case 7:
          thisData = thisPacket.sensorData8;
          break;
        case 8:
          thisData = thisPacket.sensorData9;
          break;

        case 9:
          thisData = thisPacket.sensorData10;
          break;
        case 10:
          thisData = thisPacket.sensorData11;
          break;
        case 11:
          thisData = thisPacket.sensorData12;
          break;
        case 12:
          thisData = thisPacket.sensorData13;
          break;
      }

      thisReading.vector = new Vector3(thisData.x, thisData.y, thisData.z); ;

      // time stamp is in uP milliseconds

      thisReading.sampleCount = thisPacket.sampleCount;
      thisReading.timeStamp = thisPacket.timeStamp;

      Vector3 thisNoise = new Vector3(thisData.xNoise, thisData.yNoise, thisData.zNoise);
      thisReading.noise = thisNoise;

      thisReading.scaleFactor = thisData.scaleFactor;

      /* note that the order is slightly juggled from the PCB sensor order and the directions
         of the axes are different too */
      int magnetometerNo = magnetometerOrder[idy];
      readings[magnetometerNo - 1] = thisReading;
    }
  }

  // Update is called once per frame
  void Update()
  {
    if (!hasStarted)
      return;

    if (serial == null || !serial.IsOpen)
      // this may take some time, but I guess it doesn't matter 
      openSerialPort();

    // if it can't be opened, just forget it
    if (serial == null || !serial.IsOpen)
      return;

    // send the uP the default UI settings
    if (uPIsProcessingCommands && !hasSentSmoothingFactors)
    {
      singleSensorToggleChange(singleSensorToggle.isOn);
      showReadingsToggleChange(showReadingsToggle.isOn);
      showNoiseBubblesToggleChange(showNoiseBubblesToggle.isOn);
      showDistancesToggleChange(showDistancesToggle.isOn);
      showInfluencesToggleChange(showInfluencesToggle.isOn);
      showPositionsToggleChange(showPositionsToggle.isOn);
      showPuckToggleChange(showPuckToggle.isOn);

      statisticOnValueChange(statisticDropdown.value);
      emaSliderOnValueChange(emaSlider.value);

      cutoffSliderOnValueChange(cutoffSlider.value);
      betaSliderOnValueChange(betaSlider.value);

      positionAlgorithmOnValueChange(positionAlgorithmDropdown.value);
      influenceStyleOnValueChange(influenceStyle.value);
      noOfSensorsOnValueChange(noOfSensors.value);

      triLatPowerSliderOnValueChange(triLatPowerSlider.value);

      hasSentSmoothingFactors = true;
    }

    if (!spPoll())
      return;

    // parse into the object data 'readings' array
    parseArduinoPacket(sensorPacket);

    // calculate the position vector magnitudes and the influnce factors (being 1/distance^p where p is the 'INFLUNCE_POWER'
    float sumOfInfluences = 0;
    for (int idx = 0; idx < (int)misc.NO_OF_MAGNETOMETERS; ++idx)
    {
      distancesAndInfluences[idx] = new DistanceAndInfluence();
      distancesAndInfluences[idx].magnetometerNo = idx;
      distancesAndInfluences[idx].distance= scaleToDistance(readings[idx].vector.magnitude);
      distancesAndInfluences[idx].influence=(float)(1 / Math.Pow(distancesAndInfluences[idx].distance, currentInfluenceOrder));
    }

    // sort in order of smallest distances
    Array.Copy(distancesAndInfluences, sortedDistancesAndInfluences, distancesAndInfluences.Length);
    sortedDistancesAndInfluences = distancesAndInfluences.OrderBy(e => e.distance).ToArray();

    // reduce the influence to zero for any over the specified number of sensors
    for (int idx = currentNoOfInfluenceSensors; idx < (int)misc.NO_OF_MAGNETOMETERS; ++idx)
    {
      int thisSensorNo = sortedDistancesAndInfluences[idx].magnetometerNo;
      distancesAndInfluences[thisSensorNo].influence = 0;
    }

    // pro-rate the influences to total 1
    for (int idx = 0; idx < (int)misc.NO_OF_MAGNETOMETERS; ++idx)
      sumOfInfluences += distancesAndInfluences[idx].influence;

    for (int idx = 0; idx < (int)misc.NO_OF_MAGNETOMETERS; ++idx)
      distancesAndInfluences[idx].influence /= sumOfInfluences;

    Vector3 thisPuckPositionInSensorAxes = CalculatePosition(readings);

    // OK that gave us the result in sensor/Pololu axis directions
    // sense test and convert to Unity axes
    Vector3 thisPuckPositionInUnityAxes = convertToUnityAxesAndSenseTest(thisPuckPositionInSensorAxes);
    thisPuckPositionInUnityAxes.y *= altitudeScaleFactor;

    // calculate the noise as SD of the position's length from the origin
    // if the noise is zero, we have a perfectly accurate position
    float puckDistance = thisPuckPositionInUnityAxes.magnitude;
    noise.AddResult(puckDistance);

    // display useful stats on the UI
    positionText.text = formatPosition(thisPuckPositionInUnityAxes, puckDistance, (float)noise.StandardDeviation());
    serialStats.text = "serial data - data packets  " + spBufferLineCount.ToString()+ " - messages " + noOfMessages.ToString() + " - read time (ms) " + readTimeInMS.ToString("00.00");

    // determine if the puck is or out-of-range
    bool positionIsOutOfRange = puckDistance > (float)misc.MAX_OVERALL_POSITION_LENGTH;
    if (showPuck && !positionIsOutOfRange)
    {
      puck.SetActive(true);

      Vector3 puckMovement = thisPuckPositionInUnityAxes - lastPuckPositionInUnityAxes;
      if (lastPuckPositionInUnityAxes.magnitude > 0 && puckMovement.magnitude > 0.1)
      {
        // draw a line from the origin to the calculated position
        puckVector.SetPosition(0, Vector3.zero);
        puckVector.SetPosition(1, thisPuckPositionInUnityAxes);

        // draw the puck
        puck.transform.localPosition = thisPuckPositionInUnityAxes;
      }
      lastPuckPositionInUnityAxes = thisPuckPositionInUnityAxes;
    }
    else
    {
      // hide the puck
      puck.SetActive(false);
      // while we're at it zap the noise numbers since they'll be off the scale
      noise.Zap();
    }

    // show the calculated magnetometer position vectors and their noise levels
    for(int idx=0;idx<(int)misc.NO_OF_MAGNETOMETERS;++idx)
    {
      LineRenderer thisReadingLR = readinglineRenderers[idx];
      GameObject thisMagnetometer = magnetometers[idx];
      Vector3 thisMagnetometerReading = readings[idx].vector;
      LineRenderer thisPositionLR = positionlineRenderers[idx];
      LineRenderer thisDistanceLR = distanceRenderers[idx];
      LineRenderer thisInfluneceLR = influenceRenderers[idx];
      GameObject thisNoiseObject = noiseObjects[idx];

      // reading noise
      Vector3 thisNoise = readings[idx].noise;

      // write sensor data to screen
      sensorNoise[idx].AddResult(thisMagnetometerReading.magnitude);
      double thisSensorNoise = sensorNoise[idx].StandardDeviation();
      if (idx < sensorData.Length)
        sensorData[idx].text = magnetometerNames[idx] + " reading " + formatVector(thisMagnetometerReading) + ", noise " + formatVector(thisNoise) +
                        ", sample count " + readings[idx].sampleCount + ", sd " + (double.IsNaN(thisSensorNoise) ? 0 : thisSensorNoise).ToString("0.0");

      // determine if the magnetometer is saturated or out-of-range
      bool isSaturated = magnetometerIsSaturated(thisMagnetometerReading);
      bool isOutOfRange = distancesAndInfluences[idx].distance==(int)misc.MAX_POSITION_VECTOR_LENGTH;

      // flag whether the sensor is saturated or out of range
      thisMagnetometer.GetComponent<Renderer>().material.color = isSaturated || isOutOfRange? Color.red : Color.green;

      // only show the bottom left sensor if 'single sensor' is selected
      // disable all the other objects
      if (singleSensor && idx != (int)magnetometerPositions.BOTTOM_LEFT)
      {
        // noise spheres
        thisNoiseObject.transform.localScale = Vector3.zero;
        thisNoiseObject.SetActive(false);

        thisReadingLR.enabled = false;
        thisPositionLR.enabled = false;
        thisDistanceLR.enabled = false;
        thisInfluneceLR.enabled = false;
        /*
                thisReadingLR.SetPosition(1, thisMagnetometer.transform.position);
                thisPositionLR.SetPosition(1, thisMagnetometer.transform.position);
                thisDistanceLR.SetPosition(1, thisMagnetometer.transform.position);
                thisInfluneceLR.SetPosition(1, thisMagnetometer.transform.position);
        */
        continue;
      }

      // readings
      Vector3 endOfReadingVector = thisMagnetometer.transform.position + thisMagnetometerReading / (int)misc.READING_SCALING;
      if (showReadings)
      {
        thisReadingLR.SetPosition(0, thisMagnetometer.transform.position);
        // add the reading vector to the magnetometer start position
        thisReadingLR.SetPosition(1, endOfReadingVector);
        thisReadingLR.enabled = true;
      }
      else
        thisReadingLR.enabled = false;

      // noise bubbles
      if (showNoiseBubbles && thisNoise.magnitude > 0)
      {
        thisNoiseObject.transform.localScale = thisNoise;
        thisNoiseObject.transform.position = endOfReadingVector;
        thisNoiseObject.SetActive(true);
      }
      else
      {
        // hide the object
        thisNoiseObject.transform.localScale = Vector3.zero;
        thisNoiseObject.SetActive(false);
      }

      // do influence vectors before distance vectors
      // influence vectors
      if (showInfluences)
      {
        thisInfluneceLR.SetPosition(0, thisMagnetometer.transform.position);
        thisInfluneceLR.SetPosition(1, thisMagnetometer.transform.position + (thisPuckPositionInUnityAxes - thisMagnetometer.transform.position) * distancesAndInfluences[idx].influence * (float)misc.SENSOR_SEPARATION_IN_MM / puckDistance);
        thisInfluneceLR.enabled = true;
      }
      else
        thisInfluneceLR.enabled = false;

      // distance vectors
      if (showDistances)
      {
        thisDistanceLR.SetPosition(0, thisMagnetometer.transform.position);
        if (puckDistance > 0)
        {
          // show the distances in the direction of the puck
          Vector3 puckDirectionVector = thisPuckPositionInUnityAxes - thisMagnetometer.transform.position;
          Vector3 unitPuckDirectionVector = puckDirectionVector / puckDirectionVector.magnitude;
          thisDistanceLR.SetPosition(1, thisMagnetometer.transform.position + unitPuckDirectionVector * distancesAndInfluences[idx].distance);
        }
        else
          thisDistanceLR.SetPosition(1, thisMagnetometer.transform.position);

        // mark the distances which are used in the calculation
        thisDistanceLR.material.color = sensorsUsedInCalculation[idx] ? Color.yellow : Color.blue;

        thisDistanceLR.enabled = true;
      }
      else
        thisDistanceLR.enabled = false;
    }
  }

  public float scaleToDistance(float bDash)
  {
    bool isNegative = bDash < 0;

    if (bDash == 0)
      return ((float)misc.MAX_POSITION_VECTOR_LENGTH);

    double adjustedBDash = Math.Abs(bDash) / (double)misc.SATURATION_LIMIT;

    // take the cube root as we know this is (at least theoretically) a cube law
    double thisDistance = Math.Pow(1/adjustedBDash, 1 / 3.0);

    // scale to get approximate physical accuracy
    thisDistance *= 10;

    if (thisDistance > (float)misc.MAX_POSITION_VECTOR_LENGTH)
      return (isNegative ? -(float)misc.MAX_POSITION_VECTOR_LENGTH : (float)misc.MAX_POSITION_VECTOR_LENGTH);

    if (isNegative)
      thisDistance = -thisDistance;

    if (double.IsInfinity(thisDistance) || double.IsNaN(thisDistance))
      print("Invalid position vetor length");

    return ((float)thisDistance);
  }

  Vector3 convertToUnityAxesAndSenseTest(float x,float y,float z)
  {
    // convert from magnetometer / Pololu board axes to Unity axes by flipping the y and z axes
    return (convertToUnityAxesAndSenseTest(new Vector3(x,y,z)));
  }

  Vector3 convertToUnityAxesAndSenseTest(Vector3 thisPosition)
  {
    // convert from magnetometer / Pololu board axes to Unity axes by flipping the y and z axes
    Vector3 thisConvertedPosition = new Vector3(thisPosition.x, thisPosition.z , thisPosition.y);
    if (isReasonableVector(thisConvertedPosition))
      return (thisConvertedPosition);
    else
      return (Vector3.zero);
  }

  Vector3 convertMagnetomerReadingToPositionVector(Vector3 thisReading)
  {
    // switch from magnetometer / Pololu board axes to Unity axes by flipping the y and z axes
    Vector3 thisPositionVector = new Vector3();
    thisPositionVector.x = scaleToDistance(thisReading.x);
    thisPositionVector.y = scaleToDistance(thisReading.z);
    thisPositionVector.z = scaleToDistance(thisReading.y);
    return(thisPositionVector);
  }

  Vector3 CalculatePositionUsingAverageDistances(Reading[] theseReadings)
  /* calculate position from magnetometer readings by averaging the x & y lengths weighted by an influence vector
     and averaging the two closest z lengths */
  {
    // pro-rate the sensor positions by the influnce factors
    float x = 0, y = 0, z = 0;
    for (int idx = 0; idx < (int)misc.NO_OF_MAGNETOMETERS; ++idx)
    {
      x += distancesAndInfluences[idx].influence * magnetometerCoordinates[idx].x;
      y += distancesAndInfluences[idx].influence * magnetometerCoordinates[idx].y;
    }

    // make z the average of the lowest two distances
    z = (sortedDistancesAndInfluences[0].distance + sortedDistancesAndInfluences[1].distance) / 2;

    Vector3 thisPosition = new Vector3(x * (float)misc.SENSOR_SEPARATION_IN_MM, y * (float)misc.SENSOR_SEPARATION_IN_MM,z);
    return (thisPosition);
  }

  double calculateSumOfSquaresError(Vector<double> thisGivenPoint, Reading[] theseReadings, double thisTriLatPower)
  {
    Vector3 thisGivenPointAsVector3 = new Vector3((float)thisGivenPoint[0], (float)thisGivenPoint[1], (float)thisGivenPoint[2]);
    return (calculateSumOfSquaresError(thisGivenPointAsVector3, theseReadings, thisTriLatPower));
  }

  double calculateSumOfSquaresError(Vector3 thisGivenPointAsVector3, Reading[] theseReadings, double thisTriLatPower)
  {
    // calculate the total error as sum of the squared errors between the suggested solution point and the sensors
    // - and weight this by the influence vector
    double sum = 0;
    for (int idx = 0; idx < (int)misc.NO_OF_MAGNETOMETERS; ++idx)
    {
      Vector3 thisSensorPosition = magnetometerCoordinates[idx] * (float)misc.SENSOR_SEPARATION_IN_MM;
      double thisDistance = (thisGivenPointAsVector3 - thisSensorPosition).magnitude;
      float thisInfluence = distancesAndInfluences[idx].influence;
      if (thisInfluence>0)   
        // weight the powered distances by the influence vector
        // of course, for conventional sum of squares, power would be 2
        sum += Math.Pow(Math.Abs(distancesAndInfluences[idx].distance - thisDistance), thisTriLatPower) * distancesAndInfluences[idx].influence;
    }
    return(sum);
  }

  Vector3 CalculatePosition(Reading[] theseReadings)
  {
    switch(currentPositionAlgorithm)
    {
      case positionAlgorithm.ORIGINAL:
        return (CalculatePositionOriginal(theseReadings));

      case positionAlgorithm.WEIGHTED_AVERAGE_DISTANCE:
        return (CalculatePositionUsingAverageDistances(theseReadings));

      case positionAlgorithm.BASIC_TRILATERATION:
        return (CalculatePositionUsingSimplexMinimisation(theseReadings));

      case positionAlgorithm.SEOKSEONG_JEON_TRILATERATION:
        return (CalculatePositionGsongsongTrilateration(theseReadings));

      default:
        return (Vector3.zero);
    }
  }
  
  Vector3 CalculatePositionUsingSimplexMinimisation(Reading[] theseReadings)
  {
    // specify target accuracy to 0.1mm and max 100 iterations
    // (it would be more helpful to specify a maximum time)
    NelderMeadSimplex thisSimplexSolver = new NelderMeadSimplex(0.001, 100);

    var f = new Func<Vector<double>, double>(v => calculateSumOfSquaresError(v,theseReadings,triLatPower));
    IObjectiveFunction thisObjectiveFunction=ObjectiveFunction.Value(f);

    // Vector<double> origin = DenseVector.OfArray(new double[] { 0, 0, 0 });
    // double testValue = f.Invoke(origin);

    // use the distance averaging algorithm to get the first guess
    Vector3 thisInitialGuessAsUnityVector = CalculatePositionUsingAverageDistances(theseReadings);
    double initialError = calculateSumOfSquaresError(thisInitialGuessAsUnityVector, theseReadings, triLatPower);
    Vector<double> thisInitialGuessAsMathNetVector = DenseVector.OfArray(new double[] {thisInitialGuessAsUnityVector.x, thisInitialGuessAsUnityVector.y, thisInitialGuessAsUnityVector.z });

    MinimizationResult thisResult = thisSimplexSolver.FindMinimum(thisObjectiveFunction, thisInitialGuessAsMathNetVector);
    // check that we actually converged
    if (thisResult.ReasonForExit != ExitCondition.Converged)
      return (Vector3.zero);

    Vector<double> thisMinimum = thisResult.MinimizingPoint;
    double finalError = calculateSumOfSquaresError(thisMinimum, theseReadings, triLatPower);

    // display the final error
    triLatErrorValue.text = finalError.ToString("N0");

    return (new Vector3((float)thisMinimum[0], (float)thisMinimum[1], (float)thisMinimum[2]));
  }

  Vector3 CalculatePositionGsongsongTrilateration(Reading[] theseReadings)
  // from Seokseong Jeon - looks like multilateration with minimisation by simulated annealing
  // see: https://github.com/gsongsong/mlat/tree/master/csharp
  {
    // yeah, we're always operating in 3 dimensions here
    const int NO_OF_DIMENSIONS = 3;

    Matrix<double> sensorPositions = new DenseMatrix((int)misc.NO_OF_MAGNETOMETERS, NO_OF_DIMENSIONS);
    for (int idx = 0; idx < (int)misc.NO_OF_MAGNETOMETERS; ++idx)
    {
      Vector3 thisSensorPosition = magnetometerCoordinates[idx] * (float)misc.SENSOR_SEPARATION_IN_MM;
      sensorPositions[idx,0] = thisSensorPosition.x;
      sensorPositions[idx,1] = thisSensorPosition.y;
      sensorPositions[idx,2] = thisSensorPosition.z;
    }

    // use the distance averaging algorithm to get the first guess
    Vector3 thisInitialGuessAsUnityVector = CalculatePositionUsingAverageDistances(theseReadings);
    Vector<double> thisInitialGuessAsMathNetVector = DenseVector.OfArray(new double[] {thisInitialGuessAsUnityVector.x, thisInitialGuessAsUnityVector.y, thisInitialGuessAsUnityVector.z});

    Vector<double> distancesAsMathNetVector = new DenseVector((int)misc.NO_OF_MAGNETOMETERS);
    for (int idx = 0; idx < (int)misc.NO_OF_MAGNETOMETERS; ++idx)
      distancesAsMathNetVector[idx] = distancesAndInfluences[idx].distance;

    // Define the search space boundary
    Matrix<double> bounds = new DenseMatrix(2, sensorPositions.ColumnCount);
    for (int i = 0; i < sensorPositions.ColumnCount; i++)
    {
      bounds[0, i] = sensorPositions.Column(i).Minimum();
      bounds[1, i] = sensorPositions.Column(i).Maximum();
    }
    // hard coded minimum of z search boundary
    bounds[0, sensorPositions.ColumnCount - 1] = 0;
    bounds[1, sensorPositions.ColumnCount - 1] = 100;

    // minimise using default target accuracy and maximum iterations
    MLAT.GdescentResult result = MLAT.mlat(sensorPositions, distancesAsMathNetVector, bounds);
    // unpick the result
    Vector<double> minimisationResult = result.estimator;
    Vector3 thisPosition = new Vector3((float)minimisationResult[0], (float)minimisationResult[1], (float)minimisationResult[2]);
    return (thisPosition);
  }

  Vector3 CalculatePositionOriginal(Reading[] theseReadings)
  // calculate position from magnetometer readings
  {
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // some kind of calibration can take place here

    float m1xA = Mathf.Abs(theseReadings[0].vector.x);
    float m1yA = Mathf.Abs(theseReadings[0].vector.y);
    float m1zA = Mathf.Abs(theseReadings[0].vector.z);

    float m2xA = Mathf.Abs(theseReadings[1].vector.x);
    float m2yA = Mathf.Abs(theseReadings[1].vector.y);
    float m2zA = Mathf.Abs(theseReadings[1].vector.z);

    float m3xA = Mathf.Abs(theseReadings[2].vector.x);
    float m3yA = Mathf.Abs(theseReadings[2].vector.y);
    float m3zA = Mathf.Abs(theseReadings[2].vector.z);

    float m4xA = Mathf.Abs(theseReadings[3].vector.x);
    float m4yA = Mathf.Abs(theseReadings[3].vector.y);
    float m4zA = Mathf.Abs(theseReadings[3].vector.z);

    float m5xA = Mathf.Abs(theseReadings[4].vector.x);
    float m5yA = Mathf.Abs(theseReadings[4].vector.y);
    float m5zA = Mathf.Abs(theseReadings[4].vector.z);

    //Debug.Log(m1xA + "," + m1yA + "," + m1zA + "," + m2xA + "," + m2yA + "," + m2zA + "," + m3xA + "," + m3yA + "," + m3zA + "," + m4xA + "," + m4yA + "," + m4zA + "," + m5xA + "," + m5yA + "," + m5zA);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////Summation of magnitude of x,y and z axes for each sensor/////////////////////////////////
    float totalValuesqM1, totalValuesqM2, totalValuesqM3, totalValuesqM4, totalValuesqM5;

    ///Variables for double function method
    float exponent1=0;
    float parameter1=0;
    float exponent2=0;
    float parameter2=0;
    float exponent3=0;
    float parameter3=0;
    float exponent4=0;
    float parameter4=0;
    float exponent5=0;
    float parameter5=0;

    double zdist1;
    double zdist2;
    double zdist3;
    double zdist4;
    double zdist5;

    float raddist1;
    float raddist2;
    float raddist3;
    float raddist4;
    float raddist5;

    float sensperc1;
    float sensperc2;
    float sensperc3;
    float sensperc4;
    float sensperc5;

    // calculate length of readings vector
    totalValuesqM1 = Mathf.Sqrt(m1xA * m1xA + m1yA * m1yA + m1zA * m1zA);
    totalValuesqM2 = Mathf.Sqrt(m2xA * m2xA + m2yA * m2yA + m2zA * m2zA);
    totalValuesqM3 = Mathf.Sqrt(m3xA * m3xA + m3yA * m3yA + m3zA * m3zA);
    totalValuesqM4 = Mathf.Sqrt(m4xA * m4xA + m4yA * m4yA + m4zA * m4zA);
    totalValuesqM5 = Mathf.Sqrt(m5xA * m5xA + m5yA * m5yA + m5zA * m5zA);
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////CUTTTING THE HIGH END VALUES TO REDUCE DISTORTION/.//////////////////////
    float cutoffvalue2 = 30000;   ////MAGIC VARIABLE - 50000 works very well //30000 works well also

    //Trying something else 
    if (totalValuesqM1 >= cutoffvalue2)
    {
      totalValuesqM1 = cutoffvalue2;
    }
    if (totalValuesqM2 >= cutoffvalue2)
    {
      totalValuesqM2 = cutoffvalue2;
    }
    if (totalValuesqM3 >= cutoffvalue2)
    {
      totalValuesqM3 = cutoffvalue2;

    }
    if (totalValuesqM4 >= cutoffvalue2)
    {
      totalValuesqM4 = cutoffvalue2;
    }
    if (totalValuesqM5 >= cutoffvalue2)
    {
      totalValuesqM5 = cutoffvalue2;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////Magnitude to distance conversion//////////////////////////////////////////
    ////MAG Sensor
    //float exponentvalshort1 = 3.01f;
    //float parametervalshort1 = 463280235f;
    //float exponentvallong1 = 3.23f;
    //float parametervallong1 = 1303911625f;
    //float cutoffparameter = 436.7f; 

    ////LIS Sensor
    float exponentvalshort1 = 2.69f;
    float parametervalshort1 = 764788648f;
    float exponentvallong1 = 3.04f;
    float parametervallong1 = 3745655795f;
    float cutoffparameter = 4086f;

    //Sensor1
    if (totalValuesqM1 >= cutoffparameter)
    {
      exponent1 = exponentvalshort1;
      parameter1 = parametervalshort1;
    }
    else if (totalValuesqM1 < cutoffparameter)
    {
      exponent1 = exponentvallong1;
      parameter1 = parametervallong1;
    }
    //Sensor2
    if (totalValuesqM2 >= cutoffparameter)
    {
      exponent2 = exponentvalshort1;
      parameter2 = parametervalshort1;
    }
    else if (totalValuesqM2 < cutoffparameter)
    {
      exponent2 = exponentvallong1;
      parameter2 = parametervallong1;
    }
    //Sensor3
    if (totalValuesqM3 >= cutoffparameter)
    {
      exponent3 = exponentvalshort1;
      parameter3 = parametervalshort1;
    }
    else if (totalValuesqM3 < cutoffparameter)
    {
      exponent3 = exponentvallong1;
      parameter3 = parametervallong1;
    }
    //Sensor4
    if (totalValuesqM4 >= cutoffparameter)
    {
      exponent4 = exponentvalshort1;
      parameter4 = parametervalshort1;
    }
    else if (totalValuesqM4 < cutoffparameter)
    {
      exponent4 = exponentvallong1;
      parameter4 = parametervallong1;
    }
    //Sensor5
    if (totalValuesqM5 >= cutoffparameter)
    {
      exponent5 = exponentvalshort1;
      parameter5 = parametervalshort1;
    }
    else if (totalValuesqM5 < cutoffparameter)
    {
      exponent5 = exponentvallong1;
      parameter5 = parametervallong1;
    }

    // use empirically measured reading->distance function to calculate representative distance
    zdist1 = (Math.Pow((parameter1 / totalValuesqM1), 1 / exponent1));
    zdist2 = (Math.Pow((parameter2 / totalValuesqM2), 1 / exponent2));
    zdist3 = (Math.Pow((parameter3 / totalValuesqM3), 1 / exponent3));
    zdist4 = (Math.Pow((parameter4 / totalValuesqM4), 1 / exponent4));
    zdist5 = (Math.Pow((parameter5 / totalValuesqM5), 1 / exponent5));

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////Translating zdist onto a vector 3 and adding ofset from sensor resulting from saturation////////////////////////
    //float linearoffset = 0;
    float linearoffset = 40;
    raddist1 = ((float)zdist1) - linearoffset;
    raddist2 = ((float)zdist2) - linearoffset;
    raddist3 = ((float)zdist3) - linearoffset;
    raddist4 = ((float)zdist4) - linearoffset;
    raddist5 = ((float)zdist5) - linearoffset;
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    sensperc1 = raddist1;
    sensperc2 = raddist2;
    sensperc3 = raddist3;
    sensperc4 = raddist4;
    sensperc5 = raddist5;

    // sort distances
    /////////////////////////////////Finding Max and Min of sensor readings//////////////////////////////////////////////////////////
    //append all the values to the two lists
    List<float> senspercList = new List<float>
          { sensperc1,sensperc2,sensperc3,sensperc4,sensperc5 };
    List<float> senspercList2 = new List<float>
          { sensperc1,sensperc2,sensperc3,sensperc4,sensperc5 };

    int[] maxValuesIndex = new int[5];

    for (int i = 0; i < 5; i++)
    {
      float tempMax = senspercList.Max();
      maxValuesIndex[i] = senspercList.FindIndex(tempMax.Equals);
      // Debug.Log(i + " high magnet is: " + maxValuesIndex[i].ToString());
      senspercList[maxValuesIndex[i]] = 0;
    }

    // calculate difference between max distance and individual distances
    //Debug.Log(maxValuesIndex[4] + "  ,  " + maxValuesIndex[3] + "  ,  " + maxValuesIndex[2] + "  ,  " + maxValuesIndex[1] + "  ,  " + maxValuesIndex[0]);
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////inversing the readings - MAGICAL FORMULA - TEST THIS WITH OTHER VALUES? MAX VALUE INSTEAD OF MIN VALUE///////////////
    sensperc1 = (sensperc1 - senspercList2[maxValuesIndex[0]]) * -1;
    sensperc2 = (sensperc2 - senspercList2[maxValuesIndex[0]]) * -1;
    sensperc3 = (sensperc3 - senspercList2[maxValuesIndex[0]]) * -1;
    sensperc4 = (sensperc4 - senspercList2[maxValuesIndex[0]]) * -1;
    sensperc5 = (sensperc5 - senspercList2[maxValuesIndex[0]]) * -1;
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ///sensor posiitons
    float posxsens1 = magnetometers[0].transform.localPosition.x;
    float posxsens2 = magnetometers[1].transform.localPosition.x;
    float posxsens3 = magnetometers[2].transform.localPosition.x;
    float posxsens4 = magnetometers[3].transform.localPosition.x;
    float posxsens5 = magnetometers[4].transform.localPosition.x;

    float posysens1 = magnetometers[0].transform.localPosition.z;
    float posysens2 = magnetometers[1].transform.localPosition.z;
    float posysens3 = magnetometers[2].transform.localPosition.z;
    float posysens4 = magnetometers[3].transform.localPosition.z;
    float posysens5 = magnetometers[4].transform.localPosition.z;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////calculating position//////////////////////////////////////////////////////////
    Vector3 WeightPosition = Vector3.zero;

    // pro-rate X and Y sensor positions by (maximum distance - individual distance)
    WeightPosition.x = (((posxsens1 * sensperc1) + (posxsens2 * sensperc2) + ((posxsens3 * sensperc3)) + ((posxsens4 * sensperc4)) + ((posxsens5 * sensperc5))) / 100.0f);
    WeightPosition.y = (((posysens1 * sensperc1) + (posysens2 * sensperc2) + ((posysens3 * sensperc3)) + ((posysens4 * sensperc4)) + ((posysens5 * sensperc5))) / 100.0f);

    // for z, just average the smallest two distances i.e. the two closest sensors
    WeightPosition.z = (senspercList2[maxValuesIndex[4]] + senspercList2[maxValuesIndex[3]]) / 2;

    return(WeightPosition);
  }

  public bool isReasonableVector(Vector3 thisVector)
  {
    return
      (
        !float.IsNaN(thisVector.x) &&
        !float.IsInfinity(thisVector.x) &&
        !float.IsNaN(thisVector.y) &&
        !float.IsInfinity(thisVector.y) &&
        !float.IsNaN(thisVector.z) &&
        !float.IsInfinity(thisVector.z)
      );
  }

  public bool magnetometerIsSaturated(Vector3 thisMagnetometerReading)
  {
    return(
        Math.Abs(thisMagnetometerReading.x) > (float)misc.SATURATION_LIMIT ||
        Math.Abs(thisMagnetometerReading.y) > (float)misc.SATURATION_LIMIT ||
        Math.Abs(thisMagnetometerReading.z) > (float)misc.SATURATION_LIMIT
          );
  }

  public String formatVector(Vector3 thisPosition)
  {
    return(
            "(" +
            thisPosition.x.ToString("+00000;-00000") + "," +
            thisPosition.y.ToString("+00000;-00000") + "," +
            thisPosition.z.ToString("+00000;-00000") +
            ")"
          );
  }

  public String formatPosition(Vector3 thisPosition, float length,float noise)
  {
    return
    (
      "position " +
      formatVector(thisPosition) +
      " - length " +
      length.ToString("000.0") +
      " - position noise " +
      noise.ToString("000.0") +
      " - noise % " +
      (length==0 || double.IsNaN(noise)?0:noise/length).ToString("00.0%")
      );
  }

  public bool openSerialPort()
  {
        getPortNames();
    try
    {

        #if UNITY_EDITOR_WIN
                serial = new SerialPort("\\\\.\\COM" + (int)misc.COM_PORT, (int)misc.COM_PORT_SPEED, Parity.None, 8, StopBits.One);
        #endif


        #if UNITY_EDITOR_OSX
                serial = new SerialPort("/dev/tty.SLAB_USBtoUART", (int)misc.COM_PORT_SPEED, Parity.None, 8, StopBits.One);
        #endif

      //note that Unity/Mon doesn't support DataReceived events
      serial.ReadTimeout = 10;
      if (serial.IsOpen)
        serial.Close();
      serial.Open();
      serial.DiscardInBuffer();
      serial.BaseStream.Flush();
      serialBuffer = new byte[serial.ReadBufferSize];
      uPIsProcessingCommands = false;
      hasSentSmoothingFactors = false;
      handshake();
      return (serial.IsOpen);
    }
    catch(Exception thisException)
    {
      print("Serial port failed to open - " + thisException.Message);
      return (false);
    }
  }




    void getPortNames()
    {
        int p = (int)Environment.OSVersion.Platform;
        List<string> serial_ports = new List<string>();

        // Are we on Unix?
        if (p == 4 || p == 128 || p == 6)
        {
            string[] ttys = Directory.GetFiles("/dev/", "tty.*");
            foreach (string dev in ttys)
            {
                if (dev.StartsWith("/dev/tty.*"))
                    serial_ports.Add(dev);

                print(String.Format(dev));
            }
        }
    }

    public void closeSP()
  {
    serial.Close();
  }

  // this structure matches the C++ version byte-wise
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  struct SensorOutputData
  {
    public float x, y, z;
    public float xNoise, yNoise, zNoise;
    public byte scaleFactor;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  struct SensorReadingsPacket
  {
    [MarshalAs(UnmanagedType.I1)]
    public byte d;
    [MarshalAs(UnmanagedType.I1)]
    public byte comma;
    public ushort byteCount;
    public ushort sensorCount;
    public ushort sampleCount;
    public uint timeStamp;

    // as this is a byte-wise exact buffer, these readings can't be described as an array
    public SensorOutputData sensorData1;
    public SensorOutputData sensorData2;
    public SensorOutputData sensorData3;
    public SensorOutputData sensorData4;
    public SensorOutputData sensorData5;

    public SensorOutputData sensorData6;
    public SensorOutputData sensorData7;
    public SensorOutputData sensorData8;
    public SensorOutputData sensorData9;

    public SensorOutputData sensorData10;
    public SensorOutputData sensorData11;
    public SensorOutputData sensorData12;
    public SensorOutputData sensorData13;

    [MarshalAs(UnmanagedType.I1)]
    public byte cr;
    [MarshalAs(UnmanagedType.I1)]
    public byte lf;
  };

  SensorReadingsPacket ByteArrayToNewSensorReadingsPacket(byte[] thisBuffer)
  {
    GCHandle handle = GCHandle.Alloc(thisBuffer, GCHandleType.Pinned);
    SensorReadingsPacket thisPacket;
    try
    {
      thisPacket = (SensorReadingsPacket)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(SensorReadingsPacket));
    }
    finally
    {
      handle.Free();
    }
    return(thisPacket);
  }

  bool spPoll()
  {
    // for now, only keep the last position reported from the uP
    bool hasData = false;
    spBufferLineCount = 0;
    Array.Clear(serialBuffer, 0, serialBuffer.Length);
    string[] theseLines;

    int noOfBytesRead;
    int totalNoOfBytesRead;
    int bytesToRead;
    int noOfReads;
    stopwatch.Reset();
    stopwatch.Start();

    try
    {
      // ask for updated data
      serial.WriteLine("F");
      serial.BaseStream.Flush();

      // pull everything off the line that we can
      totalNoOfBytesRead = 0;
      bytesToRead = serialBuffer.Length;
      noOfReads = 0;

      do
      {
        noOfBytesRead=serial.Read(serialBuffer, totalNoOfBytesRead, bytesToRead);
        totalNoOfBytesRead += noOfBytesRead;
        bytesToRead -= noOfBytesRead;
        if(noOfBytesRead>0)
          ++noOfReads;
        readTimeInMS = stopwatch.Elapsed.TotalMilliseconds;
      }
      while (noOfBytesRead > 0 && totalNoOfBytesRead > 0 && serialBuffer[totalNoOfBytesRead - 1]!= (char)misc.LF && bytesToRead > 0 && readTimeInMS < (double)misc.SERIAL_TIMEOUT_IN_MS);

      // now divide up into a mix of binary and text packets
      if (totalNoOfBytesRead == 0)
      {
        readTimeInMS = stopwatch.Elapsed.TotalMilliseconds;
        stopwatch.Stop();
        return (false);
      }

      noOfMessages = 0;
      int noOfBytesProcessed = 0;

      hasData = false;

      // break into CR/NL separated messages
      String thisText = System.Text.Encoding.UTF8.GetString(serialBuffer, 0, totalNoOfBytesRead);
      theseLines = thisText.Split(crNlSplit, StringSplitOptions.RemoveEmptyEntries);

      foreach (string thisLine in theseLines)
      {
        int noOfRemainingBytes = totalNoOfBytesRead - noOfBytesProcessed;
        if (noOfRemainingBytes <= 0)
          break;

        // check whether it's a data packet
        if (thisLine.Length >= 2 && thisLine[1] == (char)misc.DATA_DELIMITER_CHAR)
        {
          switch (thisLine[0])
          {
            case (char)misc.DATA_START_CHAR:
              {
                if (noOfRemainingBytes < SIZE_OF_PACKET)
                {
                  print("Partial data packet - received size " + noOfRemainingBytes + " expected size " + SIZE_OF_PACKET);
                  break;
                }

                // shuffle the buffer down to make life easier
                if (noOfBytesProcessed > 0)
                {
                  Buffer.BlockCopy(serialBuffer, noOfBytesProcessed, serialBuffer, 0, noOfRemainingBytes);
                  Array.Clear(serialBuffer, noOfRemainingBytes + 1, noOfBytesProcessed);
                }

                // unmarshal input buffer to struct
                sensorPacket = ByteArrayToNewSensorReadingsPacket(serialBuffer);
                if (sensorPacket.byteCount != SIZE_OF_PACKET)
                {
                  print("Invalid sensor packet size - received size " + sensorPacket.byteCount + " expected size " + SIZE_OF_PACKET);
                  break;
                }

                // OK we can definitely say we have a data packet
                hasData = true;
                break;
              }

            case (char)misc.NOISE_HEADINGS_START_CHAR:
              {
                print(thisLine);
                uPNoiseHeadings.text = thisLine;
                break;
              }

            case (char)misc.NOISE_START_CHAR:
              {
                print(thisLine);
                uPNoiseStats.text = thisLine;
                break;
              }

            case (char)misc.ACK_CHAR:
              {
                // do nothing
                break;
              }

            default:
              print("Unknown data packet with identifier '" + thisLine[0] + "'");
              break;
          }
        }
        else
        {
          // general debug text
          print(thisLine);
          statusBar.text = thisLine;
          break;
        }

        // note that the uP is processsing commands as soon as we have the first data packet
        if (hasData)
          uPIsProcessingCommands = true;

        ++noOfMessages;
        if (thisLine.Length >= 2 && thisLine[1] == (char)misc.DATA_DELIMITER_CHAR && thisLine[0] == (char)misc.DATA_START_CHAR)
          noOfBytesProcessed += SIZE_OF_PACKET;
        else
          noOfBytesProcessed += thisLine.Length + crNlSplit[0].Length;
        readTimeInMS = stopwatch.Elapsed.TotalMilliseconds;
      }
    }
    catch (Exception thisException)
    {
      print("Serial exception - " + thisException.Message);
    };

    readTimeInMS = stopwatch.Elapsed.TotalMilliseconds;
    stopwatch.Stop();
    return (hasData);
  }
}
