
#ifndef RESULT_H
#define RESULT_H

// to save time, don't calculate SD, if we don't need it
#undef MAX_PERF

#if defined(M0) 
  #define Serial SERIAL_PORT_USBVIRTUAL
#endif

#include <Arduino.h>

struct FilterClass
{
  bool firstTime;
public:
  FilterClass() {firstTime=true; hatxprev=0;};
  float hatxprev;
  float Filter(float x, float alpha); 
};

class Result
{
protected:
  unsigned int actualCount;
  float sum;
  float sumOfSquares;

  // mean stats
  float meanSum;
  float meanSumOfSquares;

  // sd stats
  float sdSum;
  float sdSumOfSquares;

  // peak-to-peak stats
  float minPeak;
  float maxPeak;

  // standard moving average
  static int noOfSamplesToAverage;
  float movingAverage;
  float lastMovingAverage;

  // filter time constant (use for all filters)
  static float filterTimeConstant;

  // EMA filter stat - note that all stats will use the same filter parameters
  static float emaAlpha;
  unsigned int emaCount; 
  float lastEma;
  float ema;

  // double EMA (uses the same alpha as above)
  float lastEmaDouble;
  float emaDouble;
  float emaOfEma;
  float lastEmaOfEma;

  // one Euro
  float oneEuro;

  // timestamp (all in us)
  unsigned long startTimeStamp;  
  unsigned long timeStamp;
  unsigned long lastTimeStamp;
  unsigned long sampleTime;
  
  // ACA temp
  unsigned long previousTimeStamp;

  byte dummy;

  // one Euro (don't zap these)
  static float mincutoff;
  static float beta;
  static float dcutoff;
  bool oneEuroFirstTime;
  FilterClass xfilt;
  FilterClass dxfilt;

  byte magnetometerNo;
  char dimension;

public:
  // i.e. ~1/10th a second at 30fps assuming 10 samples per frame
  enum {DEFAULT_FILTER_TIME_CONSTANT_IN_MS=33};

	Result() {Zap();oneEuroFirstTime=true;beta=0.0;mincutoff=0.001;dcutoff=0.001;};
  void Zap() {memset(&actualCount,0,(size_t)(&dummy-(byte*)&actualCount));};
  void ZapEMACount() {emaCount=0;};

  void AddResult(float value);
  float Mean();
  unsigned int Count() {return(actualCount);};
  unsigned long TimeStamp() {return(lastTimeStamp);};
  // note sample time is zero for the first reading 
  float SampleSpeedInUS() {return(actualCount>1?float(lastTimeStamp-startTimeStamp)/(actualCount-1):0);};
  
#if !defined(MAX_PERF)
  // standard population stats
  float StandardDeviation();
  float MeanSD();
  float SDSD();
  float RSESD();
  float SNR() {return(pow(fabs(Mean())/StandardDeviation(),2));};
  float SNRdB() {return(10*log10(SNR()));};
  float StandardErrorOfMean() {return(actualCount?StandardDeviation()/sqrt(actualCount):0);};
  float RelativeStandardErrorOfMean()
  {
    if(!actualCount || !Mean())
      return(0);
    return(StandardErrorOfMean()/fabs(Mean()));   
  };
#endif

  // Peak-to-peak
  float PeakToPeak();

  // standard and filter EMA
  static float SetFilterTimeConstant(int timeConstantInFrames,float thisFilterTimeConstant,float thisSampleTime);
  static float Alpha() {return(emaAlpha);};
  static float SetCutoffFrequency(float thisCutoffFrequency) {mincutoff=dcutoff=thisCutoffFrequency;};
  static float CutoffFrequency() {return(mincutoff);};
  static float SetBeta(float thisBeta) {beta=thisBeta;};
  static float Beta() {return(beta);};
  static int NoOfSamplesToAverage() {return(noOfSamplesToAverage);};
  
  float EMA() {return(ema);};  
  float EMACount() {return(emaCount);};

  float MovingAverage() {return(movingAverage);}; 
  float EMADouble() {return(emaDouble);}; 
  float OneEuro() {return(oneEuro);};

  float Alpha(float rate, float cutoff);  
  unsigned long SampleTime() {return(sampleTime);};

  void SetMagnetometerId(byte thisMagnetometerNo, char thisDimension) {magnetometerNo=thisMagnetometerNo; dimension=thisDimension;};
  void oneEuroDebug(float value,float rate, float dx, float edx, float cutoff);
};

#endif
