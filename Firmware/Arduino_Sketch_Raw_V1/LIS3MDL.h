#ifndef LIS3MDL_h
#define LIS3MDL_h

#include <Arduino.h>
#include ".\Magnetometer.h"

class LIS3MDL:public Magnetometer
{
  public:
    // register addresses
    enum registers {X_OFFSET_L_REG=0x5, X_OFFSET_H_REG, Y_OFFSET_L_REG, Y_OFFSET_H_REG, Z_OFFSET_L_REG, Z_OFFSET_H_REG,
                    WHO_AM_I_REG=0x0F, CTRL_REG1=0x20, CTRL_REG2=0x21, CTRL_REG3=0x22, CTRL_REG4=0x23,CTRL_REG5=0x24,
                    STATUS_REG=0x27, OUT_X_L=0x28, OUT_X_H=0x29, OUT_Y_L=0x2A, OUT_Y_H=0x2B, OUT_Z_L=0x2C, OUT_Z_H=0x2D,
                    TEMP_OUT_L=0x2E, TEMP_OUT_H=0x2F,
                    INT_CFG=0x30, INT_SRC=0x31, INT_THS_L=0x32, INT_THS_H=0x33};

    // Reg 1 - output mode for x,y
    enum {XY_MODE_LOW = 0x00, XY_MODE_MEDIUM = 0x20, XY_MODE_HIGH = 0x40, XY_MODE_ULTRA = 0x60};
    
    // Reg 1 - output data rates - standard and high rate (actual speed as a function of output mode)
    enum {ODR_0_625Hz = 0x00, ODR_1_25Hz  = 0x04, ODR_2_5Hz = 0x08, ODR_10Hz = 0x10, ODR_20Hz = 0x14, ODR_40Hz = 0x18, ODR_80Hz = 0x1C, FAST_ODR = 0x2};
    
    // Reg 2 - full scale
    enum {SCALE_4G = 0, SCALE_8G = 1, SCALE_12G = 2, SCALE_16G = 3, NO_OF_SCALE_FACTORS=4};
    
    // Reg 3 - output mode
    enum {OP_CONT_CONV = 0x00, OP_SNGL_CONV = 0x01, OP_IDLE = 0x11};
    
    // Reg 4 - operating mode for z
    enum {Z_MODE_LOW = 0x00, Z_MODE_MEDIUM = 0x04, Z_MODE_HIGH = 0x08, Z_MODE_ULTRA = 0x0C};
    
    enum miscCommand {SOFT_RST=0x4, REBOOT=0x8, ZYXDA=0x8, ZYXOR=0x80, ENABLE_SELF_TEST=0x1D, DISABLE_SELF_TEST=0x1C, BLOCK_DATA_UPDATE=0x40};

    // for self-test i.e. bits/gauss
    enum gain {GAIN_4G=6842, GAIN_8G=3421, GAIN_12G=2281, GAIN_16G=1711};

    enum miscAddress {LOW_I2C_ADDRESS=0x1C, HIGH_I2C_ADDRESS=0x1E, EXPECTED_WHO_ID=0x3D};

    enum timeout {SELF_TEST_TIMEOUT_IN_MS=100,RESET_TIMEOUT_IN_MS=50,MISC_IO_TIMEOUT=0};

    enum misc {BYTE_MSB=0x80};

    LIS3MDL(void);

    virtual bool init(uint8_t thisAddress=0, bool shouldLog=false);
    virtual void resetDevice();
    virtual bool selfTest(bool shouldLogResult=false);   
    virtual void setDataRate();
    virtual bool readXYZ(bool shouldLog=false);
    virtual bool checkDataReady(uint8_t thisStatus, bool shouldLog=false);
    virtual bool waitForDataReady(uint16_t timeOutInMS,bool shouldLog=false);
    virtual uint16_t getSelfTestTimeoutInUS() {return(SELF_TEST_TIMEOUT_IN_MS*1000);};
    
    // ACA
    uint8_t checkStatus(bool shouldLog=false);
    uint8_t checkStatus(char* location,bool shouldLog=false);
    // bool checkDataReady(bool shouldLog=false);
    bool checkOverrun(bool shouldLog=false);
    
    bool checkOverrun(uint8_t thisStatus, bool shouldLog=false);
    bool setOffset(vector<int32_t>& offset) {return(setOffset(offset.x,offset.y,offset.z));};
    bool setOffset(int32_t x,int32_t y,int32_t z);

    bool checkSelfTestValue(const char* dimension,int32_t thisValue,int32_t minValue,int32_t maxValue, bool shouldLogResult=true);

    // set scaling
    bool setScale(int newScale /* in Gauss */);

    void attemptToFreeStuckBus();
    bool deviceFoundOnAddress(uint8_t thisAddress, bool shouldLog=false);

    // note all values are held relative to 16G gain
    static float toGauss(float thisValueInBits) {return(thisValueInBits/float(GAIN_16G));};
    static float toTesla(float thisValueInBits) {return(toGauss(thisValueInBits)*1.0E4f);};

    // self-test limits  for +-12 gauss range (from page 19 of AN4602)
    static const vector<float> lowSelfTestLimit;
    static const vector<float> highSelfTestLimit;
    // conversion factor for 12G to 16G standard
    static const float SCALE_FACTOR_12G;
};

#endif
