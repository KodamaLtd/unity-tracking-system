#include ".\AK09940.h"
#include <Wire.h>
#include <math.h>

bool AK09940::init(uint8_t thisAddress, bool shouldLog)
{
  // actually there's only one possible address, so set it to the default
  address=I2C_ADDRESS;

  // check that we can read the baked-in company and device IDs
  uint8_t thisCompanyID=readReg(WIA1);
  bool isCompanyIDFound=thisCompanyID==COMPANY_ID;

  uint8_t thisDeviceID=readReg(WIA2);
  bool isDeviceIDFound=thisDeviceID==DEVICE_ID;
  
  if(shouldLog)
  {
    if(isCompanyIDFound)
    {
      Serial.print("Found company id ");
      Serial.print(thisCompanyID);
      Serial.print(" on address ");
      Serial.println(address);
    }
    else
    {
      Serial.println("Couldn't locate company ID"); 
      return(false);
    }

    if(isDeviceIDFound)
    {
      Serial.print("Found device id ");
      Serial.print(thisDeviceID);
      Serial.print(" on address ");
      Serial.println(address);
    }
    else
    {
      Serial.println("Couldn't locate device ID"); 
      return(false);
    }   

    float thisTemperature=getTemperatureInCelcius();
    Serial.print("Temperature is ");
    Serial.println(thisTemperature);
  }
  
  return(isCompanyIDFound && isDeviceIDFound);
}

float AK09940::getTemperatureInCelcius()
{
/* 
  // writeReg(CONTROL_0_REG, INITIATE_TEMPERATURE_MEASUREMENT);
  if(waitForTemperatureReady(TEMPERATURE_READ_TIME_IN_MS*1000))
  {
    uint8_t thisTemperatureValue=readReg(TMPS);
    return(30+thisTemperatureValue/1.72);
  }
  else
  {
    Serial.print("Failed to read temperature");
    return(0);   
  }
*/
}

void AK09940::resetDevice()
{
/*  
  // software reset
  // writeReg(CONTROL_1_REG, SW_RST);
  // page 9 of the REV C datasheet 
  delay(POWER_UP_TIME_IN_MS);

  // do both a set and a reset pulse
  // writeReg(CONTROL_0_REG, INITIATE_SET_PULSE);
  delay(SET_RESET_PULSE_WAIT_TIME_IN_MS);

  // writeReg(CONTROL_0_REG, INITIATE_RESET_PULSE);
  delay(SET_RESET_PULSE_WAIT_TIME_IN_MS);  

*/
}

void AK09940::setDataRate()
{
/*  
  // set continuous measurement rate and get an interrupt when the reading is done
  // writeReg(CONTROL_2_REG, CM_14Hz | MEASUREMENTS_COMPLETED_INT);
  
  // set to lowest noise / highest current / low output rate
  // writeReg(CONTROL_1_REG, ODR_600Hz);

  // set continuous measurements going
  // writeReg(CONTROL_0_REG, INITIATE_FIELD_MEASUREMENT);
*/
}

bool AK09940::checkDataReady(uint8_t thisStatus, bool shouldLog)
{
/*  
  // check given status
  if(shouldLog)
    Serial.println(thisStatus & FIELD_MEASUREMENT_DONE?"data ready":"no data ready");

  return(thisStatus & FIELD_MEASUREMENT_DONE);
*/
  return(false);
}

bool AK09940::checkTemperatureReady(uint8_t thisStatus, bool shouldLog)
{
/*  
  // check given status
  if(shouldLog)
    Serial.println(thisStatus & FIELD_MEASUREMENT_DONE?"data ready":"no data ready");

  return(thisStatus & TEMPERATURE_MEASUREMENT_DONE);
*/
  return(false);
}

bool AK09940::waitForDataReady(uint16_t timeOutInUS,bool shouldLog)
{
/*  
  // read and wait for status until ready (with timeout)
  unsigned long startTime=micros();
  unsigned long elapsedTime;
  bool isDataReady=false;
  
  do
  {
    uint8_t thisStatus=0; // readReg(STATUS_REG);
    if(shouldLog)
    {
      Serial.print("status ");
      Serial.println(thisStatus);
    }
    isDataReady=checkDataReady(thisStatus,shouldLog);
    elapsedTime=micros()-startTime;
    if(shouldLog)
    {
      Serial.print("read time (us) ");
      Serial.println(elapsedTime);
    }
  }
  while(!isDataReady && elapsedTime<timeOutInUS);
  
  return(isDataReady); 
*/
  return(false);
}

bool AK09940::waitForTemperatureReady(uint16_t timeOutInUS,bool shouldLog)
{
/*
  // read and wait for status until ready (with timeout)
  unsigned long startTime=micros();
  unsigned long elapsedTime;
  bool isDataReady=false;
  
  do
  {
    uint8_t thisStatus=0; // readReg(STATUS_REG);
    if(shouldLog)
    {
      Serial.print("status ");
      Serial.println(thisStatus);
    }
    isDataReady=checkTemperatureReady(thisStatus,shouldLog);
    elapsedTime=micros()-startTime;
    if(shouldLog)
      Serial.println(elapsedTime);
  }
  while(!isDataReady && elapsedTime<timeOutInUS);
  
  return(isDataReady); 
*/
  return(false);
}

bool AK09940::readXYZ(bool shouldLog)
{
/*
  // initiate a measurement
  unsigned long startTransactionTime=micros();
  // writeReg(CONTROL_0_REG, INITIATE_FIELD_MEASUREMENT);
  
  if(!waitForDataReady(DATA_READ_TIME_IN_MS*1000))
  {
    Serial.print("Failed to read data");
    return(false);
  }

  Wire.beginTransmission(address);
  // Wire.write(X_L_REG);
  Wire.endTransmission();
  
  Wire.requestFrom(address, (uint8_t)6);

  unsigned long startReceiveTime=millis();
  while (Wire.available() < 6)
  {
    if (MISC_IO_TIMEOUT>0 && (millis()-startReceiveTime)>MISC_IO_TIMEOUT)
      return(false);
  }

  uint8_t xlm = Wire.read();
  uint8_t xhm = Wire.read();
  uint8_t ylm = Wire.read();
  uint8_t yhm = Wire.read();
  uint8_t zlm = Wire.read();
  uint8_t zhm = Wire.read();

  unsigned long endTransactionTime=micros();
  if(shouldLog)
  {
    Serial.print("Data read time was ");
    Serial.print(endTransactionTime-startTransactionTime);
    Serial.println("uS");
  }
 
  // combine high and low bytes
  m.x = (uint32_t)(xhm << 8 | xlm)-32768;
  m.y = (uint32_t)(yhm << 8 | ylm)-32768;
  m.z = (uint32_t)(zhm << 8 | zlm)-32768;

  m.x-=offsetVector.x;
  m.y-=offsetVector.y;
  m.z-=offsetVector.z;
*/  
/*
  // Scale to 16G standard
  m.x*=SCALING_FACTOR_16G;
  m.y*=SCALING_FACTOR_16G;
  // our convention is that 'out-of-the-PCB' is +ve Z
  // so reverse the sign for the AK09940
  m.z*=-SCALING_FACTOR_16G;     
*/    
  return(true);
}
