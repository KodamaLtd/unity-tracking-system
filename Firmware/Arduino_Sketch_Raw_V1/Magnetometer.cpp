#include ".\Magnetometer.h"
#include <Wire.h>
#include <math.h>

void Magnetometer::vector_normalize(vector<float> *a)
{
  float mag = sqrt(vector_dot(a, a));
  a->x /= mag;
  a->y /= mag;
  a->z /= mag;
}

bool Magnetometer::calculateAverageOfNReadings(uint8_t noOfSamples,vector<int32_t>& average, bool shouldLog)
{
  // use 32-bit integers for accumulation otherwise we'll run over max 16-bit
  vector<int32_t> runningAverage={0,0,0};
  if(!noOfSamples)
    return(false);
    
  // read and discard first sample - this also ensures there's fresh data
  // prime the pump by queueing up a read request
  readXYZ();
  if(!waitForDataReady(getSelfTestTimeoutInUS(),shouldLog))
  {
    if(shouldLog)
      Serial.println("calculateAverageOfNReadings - failed to receive data");
    return(false);
  }
  readXYZ();

  // average n samples
  for(int idx=0;idx<noOfSamples;++idx)
  {
    if(shouldLog)
    {
      Serial.print("sample ");
      Serial.println(idx);
    }
    if(!waitForDataReady(getSelfTestTimeoutInUS(),shouldLog))
    {
      if(shouldLog)
        Serial.println("calculateAverageOfNReadings - failed to receive data");
      return(false);
    }    
    readXYZ();
    
    if(shouldLog)
    {
      Serial.print("point ");
      Serial.print(m.x);
      Serial.print(",");
      Serial.print(m.y);
      Serial.print(",");
      Serial.println(m.z);
    }
    runningAverage.x+=m.x;
    runningAverage.y+=m.y;
    runningAverage.z+=m.z;
  }

  runningAverage.x/=noOfSamples;
  runningAverage.y/=noOfSamples;
  runningAverage.z/=noOfSamples;

  average.x=runningAverage.x;
  average.y=runningAverage.y;
  average.z=runningAverage.z;

  if(shouldLog)
  {
    Serial.print("average ");
    Serial.print(average.x);
    Serial.print(",");
    Serial.print(average.y);
    Serial.print(",");
    Serial.println(average.z);
  }

  return(true);  
}

void Magnetometer::writeReg(uint8_t reg, uint8_t value)
{
  Wire.beginTransmission(address);
  Wire.write(reg);
  Wire.write(value);
  last_status = Wire.endTransmission();
}

uint8_t Magnetometer::readReg(uint8_t reg)
{
  uint8_t value;

  Wire.beginTransmission(address);
  Wire.write(reg);
  last_status = Wire.endTransmission();
  Wire.requestFrom(address, (uint8_t)1);
  value = Wire.read();
  // Wire.endTransmission();

  return(value);
}

bool Magnetometer::testRegExists(uint8_t thisRegister)
{
  Wire.beginTransmission(address);
  Wire.write(thisRegister);
  if (Wire.endTransmission())
    return(false);

  Wire.requestFrom(address, 1);
  return(Wire.available());
}
