#ifndef SensorOutputData_h
#define SensorOutputData_h

// because of the vagaries of the .ino pre-compile step, this can't be declared in the .ino file

// the primary result-containing object to marshall
// note these should all be on 4-byte boundaries

#pragma pack(1)

struct SensorReading
{
  float x,y,z;
  float xNoise, yNoise, zNoise;
  uint8_t scaleFactor;
};

struct SensorOutputData
{
  uint8_t d;
  uint8_t comma;
  // struct length for checking by receiving app i.e. sizeof(SensorOutputData)
  uint16_t byteCount;

  uint16_t sensorCount; 
  uint16_t sampleCount;
  uint32_t timeStamp;
  
  SensorReading readings[NO_OF_MAGNETOMETERS];
  uint8_t cr;
  uint8_t lf;
};

#endif
