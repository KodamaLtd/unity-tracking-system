#include ".\LIS3MDL.h"
#include <Wire.h>
#include <math.h>

const LIS3MDL::vector<float> LIS3MDL::lowSelfTestLimit=(LIS3MDL::vector<float>){1,1,0.1};
const LIS3MDL::vector<float> LIS3MDL::highSelfTestLimit=(LIS3MDL::vector<float>){3,3,1};
const float LIS3MDL::SCALE_FACTOR_12G=4.0f/3.0f;
    
LIS3MDL::LIS3MDL(void)
{
}

bool LIS3MDL::deviceFoundOnAddress(uint8_t thisAddress, bool shouldLog)
{
  // also,if found, leave the correct address in the sensor object
  address=thisAddress;
  if (readReg(WHO_AM_I_REG)!=EXPECTED_WHO_ID)
    address=0;

  if(shouldLog)
  {
    if(address)
      Serial.print("Device detected on address ");
    else
      Serial.print("Device not detected on address ");
    
    Serial.println(thisAddress);
  }

  return(address>0);
}

bool LIS3MDL::init(uint8_t thisAddress, bool shouldLog)
{
  bool isFound=false;
  
  if(thisAddress)
  {
    // see if I can find the device
    isFound=deviceFoundOnAddress(thisAddress, shouldLog);
  }
  // see if we can find it ourselves
  else
  {
    isFound=deviceFoundOnAddress(HIGH_I2C_ADDRESS, shouldLog);
    if(!isFound)
      isFound=deviceFoundOnAddress(LOW_I2C_ADDRESS, shouldLog);
  }

  if(shouldLog)
  {
    if(isFound)
    {   
      Serial.println("Device successfully found");
      Serial.println("Resetting device");
    }
    else
      Serial.println("Device not found");   
  }

  if(isFound)
    resetDevice();
  
  return(isFound);
}

void LIS3MDL::resetDevice()
{
  // try to return to a known state by resetting the device
  // this is 'like a power toggle'
  writeReg(CTRL_REG2, SOFT_RST);
  // the reset time isn't specified in the datasheet
  delay(RESET_TIMEOUT_IN_MS);
  // 'reload the calibration parameters'
  writeReg(CTRL_REG2, REBOOT);
  delay(RESET_TIMEOUT_IN_MS);    
}

void LIS3MDL::attemptToFreeStuckBus()
{
   Wire.beginTransmission(address);
   Wire.write(uint8_t(0));
   Wire.endTransmission();                      
}

uint8_t LIS3MDL::checkStatus(bool shouldLog)
{
  uint8_t thisStatus=readReg(STATUS_REG);
  if(shouldLog && thisStatus)
  {
    Serial.print("status ");
    Serial.println(thisStatus);
  }
  return(thisStatus);
}

uint8_t LIS3MDL::checkStatus(char* location,bool shouldLog)
{
  uint8_t thisStatus=readReg(STATUS_REG);
  if(shouldLog && thisStatus)
  {
    Serial.print(location);
    Serial.print(" status ");
    Serial.println(thisStatus);
  }
  return(thisStatus);
}

bool LIS3MDL::checkDataReady(uint8_t thisStatus, bool shouldLog)
{
  // check given status
  if(shouldLog)
    Serial.println(thisStatus & ZYXDA?"data ready":"no data ready");

  return(thisStatus & ZYXDA);
}

bool LIS3MDL::checkOverrun(uint8_t thisStatus, bool shouldLog)
{
  if(shouldLog)
  {
    Serial.print("ZYXOR - ");
    Serial.print(ZYXOR);
    Serial.println(thisStatus & ZYXOR?"overrun":"no overrun");
  }
  return(thisStatus & ZYXOR);
}

bool LIS3MDL::waitForDataReady(uint16_t timeOutInMS,bool shouldLog)
{
  // read and wait for status until ready (with timeout)
  unsigned long startTime=millis();
  unsigned long elapsedTime;
  bool isDataReady=false;
  
  do
  {
    uint8_t thisStatus=readReg(STATUS_REG);
    if(shouldLog)
    {
      Serial.print("status ");
      Serial.println(thisStatus);
    }
    isDataReady=checkDataReady(thisStatus,shouldLog);
    elapsedTime=millis()-startTime;
    if(shouldLog)
      Serial.println(elapsedTime);
  }
  while(!isDataReady && elapsedTime<timeOutInMS);
  
  return(isDataReady);
}

bool LIS3MDL::checkOverrun(bool shouldLog)
{
  uint8_t thisStatus=readReg(STATUS_REG);
  return(checkOverrun(thisStatus,shouldLog));
}

void LIS3MDL::setDataRate(void)
{
  resetDevice();

  // set the data rate, scaling and conversion (80Hz is higher than our max 60fps frame rate)
  // should lower noise and higher current
  writeReg(CTRL_REG1, FAST_ODR | XY_MODE_LOW);
  writeReg(CTRL_REG4, Z_MODE_LOW);

  // try to avoid reading mis-matching samples (this doesn't help with synchronising z)
  // page 14 of AN4062
  writeReg(CTRL_REG5, BLOCK_DATA_UPDATE);

  // start continuous conversion (which means we should watch the data ready line)
  // (also note that the mask bits must be forced 0 'for correct functioning of the device' p26)
  writeReg(CTRL_REG3, OP_CONT_CONV);

  // power up, wait 20ms and discard first sample
  // (actually the max time to switch out of idle mode should be 7ms)
  delay(20);
  readXYZ();   
}

bool LIS3MDL::setScale(int newScale)
{
  int reg2Pattern=0;
  switch(newScale)
  {
    case 4:
      reg2Pattern=SCALE_4G;
      break;
      
    case 8:
      reg2Pattern=SCALE_8G;
      break;

    case 12:
      reg2Pattern=SCALE_12G;
      break;

    case 16:
      reg2Pattern=SCALE_16G;
      break;

    default:
      return(false);
  }
    
  writeReg(CTRL_REG2, reg2Pattern << 5);
  // delay of 20ms taken from AN4602 self-test example
  delay(20);
  currentScale=newScale;
  return(true);
}

bool LIS3MDL::checkSelfTestValue(const char* dimension,int32_t thisValue,int32_t minValue,int32_t maxValue, bool shouldLogResult)
{
  bool result=thisValue>=minValue && thisValue<=maxValue;

  if(shouldLogResult)
  {
    Serial.print("Test value ");
    Serial.print(dimension);
    Serial.print("=");
    Serial.print(thisValue);
    Serial.print(" against range ");
    Serial.print(minValue);
    Serial.print("-");
    Serial.print(maxValue);
    Serial.print(" result ");
    Serial.println(result?"true":"false");
  }
 
  return(result);
}

bool LIS3MDL::selfTest(bool shouldLogResult)
{
  // this start-up sequence uses page 19 of AN4602
  // the device needs a constant magnetic field (i.e. preferably just background) during the test
  
  // set the data rate, scaling and conversion
  writeReg(CTRL_REG1, ODR_80Hz | XY_MODE_ULTRA);
  writeReg(CTRL_REG4, Z_MODE_ULTRA);
  // gauss scale (also note that the mask bits must be forced 0 'for correct functioning of the device')
  setScale(12);

  // start continuous conversion (which means we should watch the data ready line/bit)
  writeReg(CTRL_REG3, OP_CONT_CONV);

  // power up, wait 20ms and get first average (actually page 10 of AN4602 suggests max turn-on time is 7ms)
  delay(20);  
  vector<int32_t> firstAverage;
  if(shouldLogResult)
    Serial.println("Calculate pre-self-test average:");
  if(!calculateAverageOfNReadings(5,firstAverage,shouldLogResult))
    return(false);

  // run self test
  writeReg(CTRL_REG1, ENABLE_SELF_TEST);
  delay(60);  

  // get second average
  vector<int32_t> secondAverage;
  if(shouldLogResult)
    Serial.println("Calculate post-self-test average:");
  if(!calculateAverageOfNReadings(5,secondAverage,shouldLogResult))
    return(false);

  // disable self test & power down
  writeReg(CTRL_REG1, DISABLE_SELF_TEST);
  writeReg(CTRL_REG3, OP_IDLE);

  // check the the difference between the averaged readings before and after self test are between prescribed datasheet limits
  static int32_t lowXLimit=lowSelfTestLimit.x*GAIN_12G*SCALE_FACTOR_12G;
  static int32_t lowYLimit=lowSelfTestLimit.y*GAIN_12G*SCALE_FACTOR_12G;
  static int32_t lowZLimit=lowSelfTestLimit.z*GAIN_12G*SCALE_FACTOR_12G;
  static int32_t highXLimit=highSelfTestLimit.x*GAIN_12G*SCALE_FACTOR_12G;
  static int32_t highYLimit=highSelfTestLimit.y*GAIN_12G*SCALE_FACTOR_12G;
  static int32_t highZLimit=highSelfTestLimit.z*GAIN_12G*SCALE_FACTOR_12G;
  
  bool passesChecks=
    checkSelfTestValue("x",abs(secondAverage.x-firstAverage.x),lowXLimit,highXLimit) &&
    checkSelfTestValue("y",abs(secondAverage.y-firstAverage.y),lowYLimit,highYLimit) &&
    checkSelfTestValue("z",abs(secondAverage.z-firstAverage.z),lowZLimit,highZLimit);

  if(shouldLogResult)
    Serial.println(passesChecks?"passed self-test checks":"didn't pass self-test checks");
  return(passesChecks);
}

bool LIS3MDL::setOffset(int32_t x,int32_t y,int32_t z)
{
  // re-scale - we can't use straightforward bit shifts as some values will be negative

  switch(currentScale)
  {
    case SCALE_4G:
      m.x/4;
      m.y/4;
      m.z/4;     
      break;

    case SCALE_8G:
      m.x/2;
      m.y/2;
      m.z/2;     
      break;

    case SCALE_12G:
      m.x/=SCALE_FACTOR_12G;
      m.y/=SCALE_FACTOR_12G;
      m.z/=SCALE_FACTOR_12G;
      break;
    
    case SCALE_16G:
      // do nothing
      break;
  }
  
  writeReg(X_OFFSET_L_REG,x & 0xFF);
  writeReg(X_OFFSET_H_REG,x >> 8 & 0xFF);
  writeReg(Y_OFFSET_L_REG,y & 0xFF);
  writeReg(Y_OFFSET_H_REG,y >> 8 & 0xFF);
  writeReg(Z_OFFSET_L_REG,z & 0xFF);
  writeReg(Z_OFFSET_H_REG,z >> 8 & 0xFF);
}

// Reads the 3 mag channels and stores them in vector m
bool LIS3MDL::readXYZ(bool shouldLog)
{
  Wire.beginTransmission(address);
  // assert MSB to enable subaddress updating
  Wire.write(OUT_X_L | BYTE_MSB);
  Wire.endTransmission();
  Wire.requestFrom(address, (uint8_t)6);

  unsigned long startTime=millis();
  while (Wire.available() < 6)
  {
    if (MISC_IO_TIMEOUT>0 && (millis()-startTime)>MISC_IO_TIMEOUT)
      return(false);
  }

  uint8_t xlm = Wire.read();
  uint8_t xhm = Wire.read();
  uint8_t ylm = Wire.read();
  uint8_t yhm = Wire.read();
  uint8_t zlm = Wire.read();
  uint8_t zhm = Wire.read();
 
  // combine high and low bytes
  m.x = (int16_t)(xhm << 8 | xlm);
  m.y = (int16_t)(yhm << 8 | ylm);
  m.z = (int16_t)(zhm << 8 | zlm);

  // Scale by the full scale value to get a comparable reading
  // (so that the reading is always relative to the 16G scaling).
  // As the noise level is ~22bits, it's fine to lose the last 2 of 32 bits.
  // Note that we can't use straightforward bit shifts as some values will be negative.
  switch(currentScale)
  {
    case SCALE_4G:
      m.x*4;
      m.y*4;
      m.z*4;     
      break;

    case SCALE_8G:
      m.x*2;
      m.y*2;
      m.z*2;     
      break;

    case SCALE_12G:
      m.x*=SCALE_FACTOR_12G;
      m.y*=SCALE_FACTOR_12G;
      m.z*=SCALE_FACTOR_12G;
      break;
    
    case SCALE_16G:
      // do nothing
      break;
  }
    
  return(true);
}
