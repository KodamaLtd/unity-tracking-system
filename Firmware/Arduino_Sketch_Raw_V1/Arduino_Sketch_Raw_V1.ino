#include "Wire.h"

// sensors
#include ".\LIS3MDL.h"
#include ".\MMC5883MA.h"
#include ".\AK09940.h"

#include ".\Result.h"
#include <cstdarg>
#include <limits.h>

#define NO_OF(x) (sizeof(x)/sizeof(x[0]))
#define ZAP(x) (memset(x,0,sizeof(x)))
typedef void (*FUNCTOR)();

// prototypes for default arguments
Magnetometer* selectMagnetometer(uint8_t thisMagnetometerNo, bool shouldLog=false);

// right now there are two board variations right now and two magnetometer choices`
enum sensor {UNKNOWN_SENSOR=0, LIS3MDL_SENSOR, MMC5883MA_SENSOR, AK09940_SENSOR, CURRENT_DEVICE=LIS3MDL_SENSOR};
char* sensorName[]={"Unknown","LIS3MDL","MMC5883MA","AK09940"};
enum board {UNKNOWN_BOARD=0, BOARD_1_6, BOARD_1_7, CURRENT_BOARD=BOARD_1_7};
char* boardName[]={"Unknown","1.6","1.7"};
enum layout {NO_OF_MAGNETOMETERS=13, /* 5 for rev 1.6, or 5/9/13 for rev 1.7 */
             NO_OF_I2C_MUXES=2, NO_OF_SENSORS_ON_FIRST_MUX=5,INTERRUPT_MUX_ADDRESS=0x20};
// true for both board variations (the 1.6 board only has one mux)
int muxAddresses[]={0x70,0x71};

// include here so it can pick-up 'NO_OF_MAGNETOMETERS'
#include ".\SensorOutputData.h"

enum misc {NO_OF_CALIBRATION_READINGS=5, TARGET_FRAME_RATE=30,MAX_INCOMING_COMMAND_SIZE=20,
           // see page 7 of TCA9548A datasheet
           MUX_RESET_PULSE_LOW_TIMING_IN_MS=1, MUX_RESET_RECOVERY_TIME_IN_MS=1,
           // see page 23 of TCA9548A datasheet
           // plus the knowledge that the 270 ohm bleed resistor pulls the power down in ~60ms
           MUX_POWER_RESET_TIME_IN_MS=100, 
           // note that the 270 ohm bleed resistor pulls the 4.2V power down to 1V in ~150ms
           SENSOR_POWER_RESET_TIME_IN_MS=250,
           SHOULD_CHANGE_SCALE=false,USE_TIMING_PINS=true,SEND_SERIAL_DATA_IN_BINARY=true,SEND_STATS=true,
           SEND_DETAILED_DATA=false,USE_SENSOR_INTERRUPTS=false};

// misc enums & look-ups
enum chars {CR=13,LF=10,NO_OF_TERMINATOR_CHARACTERS};
enum statistic {MEAN=0,MOVING_AVERAGE,EMA,EMA_DOUBLE,ONE_EURO};
char* statisticName[]={"Mean","Moving Average","Exponential Moving Average","Double EMA","One Euro"};

// board specific enums 

// 1.6 board / BlueFruit nRF52
/*
enum pins {GREEN_LED=27, ORANGE_LED=30, YELLOW_LED=31,
           MUX_RESET=A0, SENSOR_POWER_PIN=16,
           SENSOR_SCAN_PIN=11, NEW_FRAME_PIN=7, SENSOR_READ_PIN=15};
*/

// 1.7 board / BlueFruit nRF52
enum pins {GREEN_LED=27, ORANGE_LED=30, YELLOW_LED=31,
           MUX_RESET=A0, MUX_POWER=13, SENSOR_POWER_PIN=14,
           SENSOR_SCAN_PIN=11, NEW_FRAME_PIN=7, SENSOR_READ_PIN=15, SENSOR_RESET=16,
           DRDY_INT=12};

// interrupt pins corresponding the magnetometers
// board 1.6 - e.g. mag1 is on A1 (board 1.7 interrupts are all via a PCA9555PW)
byte magnetometerToInterruptPin[]={A1,A4,A2,A5,A3};
// board 1.6
byte magnetometerToMuxAddress[]={3,2,6,5,4,};
// board 1.6 & 1.7
char* magnetometerName[]={"BL","TL","BR","TR","MI","LC","TC","RC","BC","BLI","TLI","TRI","BRI"};

// app state
static volatile bool inSetup = false;
static volatile bool isProcessingData = false;

// the stat to return to the UI
int chosenStatistic=MEAN;
int timeConstantInFrames;
String incomingCommand;

Magnetometer* magnetometers[NO_OF_MAGNETOMETERS];

// one interrupt routine for each magnetometer
FUNCTOR dataReadyRoutines[NO_OF_MAGNETOMETERS];
// interrupt flags
volatile bool isDataReady[]={false,false,false,false,false};

// results
Magnetometer::vector<Result> resultVector[NO_OF_MAGNETOMETERS];
Result noiseCalc;

// timing etc stats
static volatile unsigned long noOfSamples=0;
// in MS since start
volatile unsigned long runTime=0;
volatile unsigned long lastRunTime=0;
volatile bool isHandlingUI=false;
volatile float sampleTimeInUS=0;

// command handling from app
enum commands {NEW_FRAME=0,RECALIBRATE,MAGNETOMETER_SETTINGS,EMA_PERIOD,STATISTIC,HANDSHAKE,TEMPERATURE,XYZ_READING,CUTOFF,BETA};
char* commandText[]={"F","R","S","E,","ST,","H","T","X","C","B"};

// binary serial output buffer
static union
{
  SensorOutputData serialOutputData;
  byte serialOutputBuffer[sizeof(SensorOutputData)];
};

// error handlers
void HardFault_Handler()
{
  HardError("* unknown hard fault *");
}

char message[128];

void HardError(const char* formatText,...)
// report error message and loop forever
{
  /*
  va_list parameterList;
  // format the message using the extra parameters  
  va_start(parameterList,formatText);
  sprintf(message,formatText,parameterList);
  */
  
  Serial.println("* Hard Error *");
  Serial.println(formatText);
  Serial.flush();
  
  bool flash = true;
  while (true)
  {
    digitalWrite(ORANGE_LED, flash);       
    flash = !flash;
    delay(1000);
  }
}

void printHexAddress(byte thisAddress)
{ 
  if (thisAddress<16)
    Serial.print("0x0");
  else
    Serial.print("0x");
  
  Serial.println(thisAddress,HEX);
}

int i2cScan(int expectedNoOfDevices, byte* addresses)
{
  byte error, address;
  int nDevices;
 
  Serial.println("scanning i2c bus for devices...");

  for(int idx=0;idx<expectedNoOfDevices;++idx)
    addresses[idx]=0;
 
  nDevices=0;
  for(address=1;address<127;++address)
  {
    Wire.beginTransmission(address);
    delayMicroseconds(10);
    error = Wire.endTransmission();
 
    if (error == 0)
    {
      Serial.print("device found at address ");
      printHexAddress(address);
      
      if(nDevices<=expectedNoOfDevices)
        addresses[nDevices]=address;

      ++nDevices;
    }
    else if (error==4)
    {
      Serial.print("error at address ");
      printHexAddress(address);
    }
  }

  Serial.print(nDevices);
  Serial.println(" i2c devices found");
  return(nDevices);
}

Magnetometer* selectMagnetometer(uint8_t thisMagnetometerNo, bool shouldLog)
// select mux address from magnetometer number (0-13)
{
  static int lastMagnetometerNo=-1;
  static int lastMuxAddress=-1;
  int returnValue=0;
  
  if (thisMagnetometerNo>=NO_OF_MAGNETOMETERS)
    HardError("Invalid magnetometer number");

  // for speed, don't switch if we're asking for the same magnetometer as last time
  if(thisMagnetometerNo!=lastMagnetometerNo)
  {
    // board 1.6 & 1.7
    uint8_t thisMuxAddress=thisMagnetometerNo<NO_OF_SENSORS_ON_FIRST_MUX?muxAddresses[0]:muxAddresses[1];

    // if there is more than one mux, turn the other off
    if(NO_OF_I2C_MUXES>1 && thisMuxAddress!=lastMuxAddress)
    {
      uint8_t thisOtherMuxAddress=thisMagnetometerNo<NO_OF_SENSORS_ON_FIRST_MUX?muxAddresses[1]:muxAddresses[0];
      Wire.beginTransmission(thisOtherMuxAddress);
      Wire.write((uint8_t)0);
      if(Wire.endTransmission())
        HardError("Mux select i2c error");

      if(shouldLog)
      {
        Serial.print("Deselect mux ");
        printHexAddress(thisOtherMuxAddress);
      }
      lastMuxAddress=thisMuxAddress;
    }

    Wire.beginTransmission(thisMuxAddress);

    uint8_t thisSensorAddress=0;
    if(thisMagnetometerNo<NO_OF_SENSORS_ON_FIRST_MUX)
      // board 1.6 & 1.7
      thisSensorAddress=magnetometerToMuxAddress[thisMagnetometerNo];
    else
      // board 1.7 - so e.g. sensor # 6 maps to channel 0 on the second mux
      // (see schematic)
      thisSensorAddress=thisMagnetometerNo-NO_OF_SENSORS_ON_FIRST_MUX;
    
    Wire.write(1 << thisSensorAddress);
    if(Wire.endTransmission())
      HardError("Mux select i2c error");

    if(shouldLog)
    {
      Serial.print("Select mux ");
      printHexAddress(thisMuxAddress);

      Serial.print("Select sensor ");
      printHexAddress(thisSensorAddress);
    }   

    lastMagnetometerNo=thisMagnetometerNo;
    // no need for any delay
  }

  return(magnetometers[thisMagnetometerNo]);
}
  
void setupInterrupts()
// for all magnetometers
{
  Serial.println("Setup magnetometer interrupts");
  Serial.flush();

  dataReadyRoutines[0]=dataReady0;
  dataReadyRoutines[1]=dataReady1;
  dataReadyRoutines[2]=dataReady2;
  dataReadyRoutines[3]=dataReady3;
  dataReadyRoutines[4]=dataReady4;

  for(int idx=0;idx<NO_OF_MAGNETOMETERS;++idx)
  {
    int thisPin=magnetometerToInterruptPin[idx];

    Serial.print("Set-up interrupt for magnetometer ");
    Serial.print(idx);
    Serial.print("/");
    Serial.print(magnetometerName[idx]);
    Serial.print(" on interrupt pin ");
    Serial.println(thisPin);

    pinMode(thisPin, INPUT_PULLUP);
    byte thisInterrupt = digitalPinToInterrupt(thisPin);
    attachInterrupt(thisInterrupt, dataReadyRoutines[idx], RISING);
  }
}

void dataReady0()
{
  isDataReady[0]=true;  
}

void dataReady1()
{
  isDataReady[1]=true;  
}

void dataReady2()
{
  isDataReady[2]=true;  
}

void dataReady3()
{
  isDataReady[3]=true;  
}

void dataReady4()
{
  isDataReady[4]=true;  
}

bool initialiseMagnetometer(uint8_t thisMagnetometerNo)
{
  Serial.print("Initialise magnetometer "); 
  Serial.print(thisMagnetometerNo);
  Serial.print("/");
  Serial.println(magnetometerName[thisMagnetometerNo]);

  Magnetometer* thisMagnetometer=selectMagnetometer(thisMagnetometerNo,true);
  // Serial.println("attempt to free bus");
  // thisMagnetometer.attemptToFreeStuckBus();

  Serial.println("init");  
  if(!thisMagnetometer->init(0,true))
  {
    HardError("Magnetometer failed initialisation");
    return(false);
  }

  // Serial.println("resetDevice"); 
  thisMagnetometer->resetDevice();
  
  Serial.println("selfTest"); 
  if(!thisMagnetometer->selfTest(false))
  {
    HardError("Magnetometer failed self test");
    return(false);
  }

  Serial.println("setScale");
  // otherwise it'll be left at 12G sclaing from the self-test
  if(CURRENT_DEVICE==LIS3MDL_SENSOR)
    ((LIS3MDL*)thisMagnetometer)->setScale(8);

  Serial.println("setDataRate"); 
  thisMagnetometer->setDataRate();

  return(true);
}

bool initialiseMagnetometers()
{
  for(int idx=0;idx<NO_OF_MAGNETOMETERS;++idx)
    switch(CURRENT_DEVICE)
    {
      case LIS3MDL_SENSOR:
        magnetometers[idx]=new LIS3MDL();
        break;

      case MMC5883MA_SENSOR:
        magnetometers[idx]=new MMC5883MA();
        break;

      case AK09940_SENSOR:
        magnetometers[idx]=new AK09940();
        break;

      default:
        HardError("Unknown device");
    }

  if(CURRENT_BOARD==BOARD_1_7)
  {
    // blip the power to the muxes sensors to properly reset them
    Serial.println("cycle mux power"); 
    digitalWrite(MUX_POWER, true);  
    delay(MUX_POWER_RESET_TIME_IN_MS);
    digitalWrite(MUX_POWER, false);     
  }
  
  // then hit the mux reset pin for good measure
  Serial.println("reset mux"); 
  digitalWrite(MUX_RESET, false);  
  delay(MUX_RESET_PULSE_LOW_TIMING_IN_MS);
  digitalWrite(MUX_RESET, true);  
  delay(MUX_RESET_RECOVERY_TIME_IN_MS);

  /* then blip the power to the sensors to properly reset them - 
     it takes about 20ms for the rail to disappear due to capacitance
     on the rails. This is inverted, so low=powered. */
  Serial.println("cycle sensor power");
  // tri-state to allow it to be pulled up towards 4.2V (actually to 3.7V)
  pinMode(SENSOR_POWER_PIN,INPUT);
  delay(SENSOR_POWER_RESET_TIME_IN_MS);
  pinMode(SENSOR_POWER_PIN,OUTPUT);
  digitalWrite(SENSOR_POWER_PIN, false);

  Wire.begin();
  Wire.setClock(400000L);

  // scan to confirm everything is there as expected
  // app specific # of i2c addresses expected
  int expectedNoOfDevices=NO_OF_I2C_MUXES+1;
  byte devices[expectedNoOfDevices];
  int noOfDevicesFound=i2cScan(expectedNoOfDevices,(byte*)&devices);

  if(CURRENT_BOARD==BOARD_1_6)
    if(noOfDevicesFound!=NO_OF_I2C_MUXES || devices[0]!=muxAddresses[0])
      HardError("Incorrect i2c devices found for version 1.6 board");
  
  if(CURRENT_BOARD==BOARD_1_7)
    if(noOfDevicesFound!=NO_OF_I2C_MUXES+1 || devices[0]!=INTERRUPT_MUX_ADDRESS ||
       devices[1]!=muxAddresses[0] || devices[2]!=muxAddresses[1])
      HardError("Incorrect i2c devices found for version 1.7 board");
  
  // otherwise
  Serial.println("Correct i2c devices found on board"); 

  // initialize each device
  for(int idx=0;idx<NO_OF_MAGNETOMETERS;++idx)
    if(!initialiseMagnetometer(idx))
      return(false);

  return(true);
}

Magnetometer* readMagnetometer(uint8_t thisMagnetometerNo,bool shouldLog=false)
{
  // the x,y,z results end up in the magnetometer object
  Magnetometer* thisMagnetometer=selectMagnetometer(thisMagnetometerNo);

  /* note that the results will be scaled by the full scale setting to get
     value relative to the 4G value */
  //if(!thisMagnetometer->waitForDataReady(1,false))
  //  Serial.println("Sensor not ready");
  thisMagnetometer->readXYZ(shouldLog);

  if(shouldLog)
  {
    Serial.print(thisMagnetometer->m.x);
    Serial.print(",");
    Serial.print(thisMagnetometer->m.y);
    Serial.print(",");
    Serial.print(thisMagnetometer->m.z);
    Serial.println();
  }
  
  return(thisMagnetometer);
}

void printVector(Magnetometer::vector<int32_t>& thisResult,bool shouldAddEndOfLine)
{ 
  Serial.print(thisResult.x);
  Serial.print(",");
  Serial.print(thisResult.y);
  Serial.print(",");
  Serial.print(thisResult.z);
  // handle the end of line
  if(shouldAddEndOfLine)
    Serial.println();
  else
    Serial.print(",");
}

bool calibrateMagnetometers()
{
  Serial.println("Calibrate zero level");
  Magnetometer::vector<int32_t> calibrationVector={0,0,0};
  
  for(int idx=0;idx<NO_OF_MAGNETOMETERS;++idx)
  {
    // give some visual indication of progress
    digitalWrite(YELLOW_LED, false);

    Magnetometer* thisMagnetometer=selectMagnetometer(idx);  

    if(!thisMagnetometer->calculateAverageOfNReadings(NO_OF_CALIBRATION_READINGS,calibrationVector,false))
      return(false);

    Serial.print("Calibration vector for magnetometer ");
    Serial.println(idx);
    printVector(calibrationVector,true);   
        
    thisMagnetometer->setOffset(calibrationVector);

    digitalWrite(YELLOW_LED, true);
    
    if(!thisMagnetometer->calculateAverageOfNReadings(NO_OF_CALIBRATION_READINGS,calibrationVector,false))
      return(false);
    Serial.print("Zero vector for magnetometer ");
    Serial.println(idx);
    printVector(calibrationVector,true);       
  }
}

void ZapResultVector(Magnetometer::vector<Result>& thisResult)
{
  if(chosenStatistic==MEAN)
  {
    thisResult.x.Zap();
    thisResult.y.Zap();
    thisResult.z.Zap();
  }
  else
  {
    // standard or filter EMA
    thisResult.x.ZapEMACount();
    thisResult.y.ZapEMACount();
    thisResult.z.ZapEMACount();
  }
}

void setup()
{
  inSetup = true;

  digitalWrite(MUX_RESET, true);  
  pinMode(MUX_RESET, OUTPUT);
  // power control for sensors, muxes etc - this are inverting so low=powered
  digitalWrite(MUX_POWER, false);  
  pinMode(MUX_POWER, OUTPUT);
  digitalWrite(SENSOR_POWER_PIN, false);  
  pinMode(SENSOR_POWER_PIN, OUTPUT);

  // setup pins
  digitalWrite(GREEN_LED, false);  
  // show that we're in start-up
  digitalWrite(ORANGE_LED, false);  
  digitalWrite(YELLOW_LED, true);  
  pinMode(GREEN_LED, OUTPUT);
  pinMode(ORANGE_LED, OUTPUT);
  pinMode(YELLOW_LED, OUTPUT);
 
  // Serial.begin(115200);
  Serial.begin(1000000); 
  while(!Serial);

  Serial.println("*** Kodama rev 1.7 ***");
  Serial.flush();
 
  Serial.println("Sensor data format 1.0");

  Serial.print("Sensor is ");
  Serial.println(sensorName[CURRENT_DEVICE]);

  Serial.print("Board version is ");
  Serial.println(boardName[CURRENT_BOARD]);

  Serial.print(NO_OF_MAGNETOMETERS);
  Serial.println(NO_OF_MAGNETOMETERS==1?" magnetometer":" magnetometers");

  Serial.print(NO_OF_I2C_MUXES);
  Serial.println(NO_OF_I2C_MUXES==1?" mux":" muxes");

  Serial.print("Size of sensor data packet is ");
  Serial.println(sizeof(SensorOutputData));

  // label up results for debugging
  for(int idx=0;idx<NO_OF_MAGNETOMETERS;++idx)
  {
    resultVector[idx].x.SetMagnetometerId(idx,'X');
    resultVector[idx].y.SetMagnetometerId(idx,'Y');
    resultVector[idx].z.SetMagnetometerId(idx,'Z');
  }
  
  // avoid re-allocating the incoming command string
  incomingCommand.reserve(MAX_INCOMING_COMMAND_SIZE);
  
  // timing debug pins - loop and frame indicators
  if(USE_TIMING_PINS)
  {
    digitalWrite(SENSOR_SCAN_PIN, true);  
    pinMode(SENSOR_SCAN_PIN, OUTPUT); 
    digitalWrite(NEW_FRAME_PIN, true);  
    pinMode(NEW_FRAME_PIN, OUTPUT);
    digitalWrite(SENSOR_READ_PIN, true);  
    pinMode(SENSOR_READ_PIN, OUTPUT);
  }
   
  Serial.println("initialise magnetometers");
  if(!initialiseMagnetometers())
    HardError("Failed to initialise magnetometers");

  Serial.println("calibrate magnetometers");
  if(!calibrateMagnetometers())
    HardError("Failed to calibrate magnetometers");
      
  // Zap result vectors
  for(int idx=0;idx<NO_OF_MAGNETOMETERS;++idx)
    ZapResultVector(resultVector[idx]);
  
  // set-up interrupts only when we've finished the calibration
  // (there's a prettier way to do this)
  if(USE_SENSOR_INTERRUPTS)
  {
    dataReadyRoutines[0]=dataReady0;
    dataReadyRoutines[1]=dataReady1;
    dataReadyRoutines[2]=dataReady2;
    dataReadyRoutines[3]=dataReady3;
    dataReadyRoutines[4]=dataReady4; 
    setupInterrupts();
  }
  
  // read once to trigger the next data ready
  for(int idx=0;idx<NO_OF_MAGNETOMETERS;++idx)
    readMagnetometer(idx,false);

  Serial.println("set default filter time constant");
  Result::SetFilterTimeConstant(1,3.0f/30.0f,0.01);

  // show that we started-up OK
  digitalWrite(GREEN_LED, true);  
  digitalWrite(YELLOW_LED, false);           
  Serial.println("start-up OK");
  inSetup = false;
}

int getIncomingCommandFromApp()
{
  incomingCommand=Serial.readStringUntil('\n');
  // ignore the terminator character
  int maxSize=min(incomingCommand.length(),MAX_INCOMING_COMMAND_SIZE);
  if(incomingCommand.length()>1)
  {
    char finalCharacter=incomingCommand[incomingCommand.length()-1];
    bool hasTerminatorCharacter=finalCharacter==CR || finalCharacter==LF;
    if(hasTerminatorCharacter)
    {
      incomingCommand[incomingCommand.length()-1]=0;
      --maxSize;
    }
  }
  return(maxSize);
}

bool matchesCommand(String& thisIncomingCommand, char* commandToMatch,int maxSize)
{
  return(!strncasecmp(thisIncomingCommand.c_str(),commandToMatch,maxSize));
}

void changeMagnetometerScaling()
{  // dynamically change the magnetometer's scale to get maxiumum sensitivity and range
  for(int thisMagnetometerNo=0;thisMagnetometerNo<NO_OF_MAGNETOMETERS;++thisMagnetometerNo)
  {
    // first get the maximum reading for all dimensions
    float x=fabs(resultVector[thisMagnetometerNo].x.Mean());
    float y=fabs(resultVector[thisMagnetometerNo].y.Mean());
    float z=fabs(resultVector[thisMagnetometerNo].z.Mean());

    float maxDimension=max(max(x,y),z);

    // if we're over 3/4 full scale (of the current scaling) then decrease the sensitivity if we can
    if(maxDimension>(SHRT_MAX*0.75))
      if(magnetometers[thisMagnetometerNo]->currentScale==4)
      {
        magnetometers[thisMagnetometerNo]->setScale(16);
        Serial.print("changing scale for magnetometer ");
        Serial.print(thisMagnetometerNo);
        Serial.println(" from 4G to 16G");
      }     
/*
    if(maxDimension>(SHRT_MAX*1.5))
      // only scale by a factor of 2 though
      if(magnetometers[thisMagnetometerNo].currentScale==8)
      {
        magnetometers[thisMagnetometerNo].setScale(16);        
        Serial.print("changing scale for magnetometer ");
        Serial.print(thisMagnetometerNo);
        Serial.println(" from 8G to 16G");
      }
*/
    // if we're under 1/2 full scale (of the scaling below) then increase the sensitivity if we can
    if(maxDimension<(SHRT_MAX*0.5))
      if(magnetometers[thisMagnetometerNo]->currentScale==16)
      {
        magnetometers[thisMagnetometerNo]->setScale(4);
        Serial.print("changing scale for magnetometer ");
        Serial.print(thisMagnetometerNo);
        Serial.println(" from 16G to 4G");
      }
/*      
    if(maxDimension<(SHRT_MAX*0.875))
      // only scale by a factor of 2 though
      if(magnetometers[thisMagnetometerNo].currentScale==8)
      {
        magnetometers[thisMagnetometerNo].setScale(4G);        
        Serial.print("changing scale for magnetometer ");
        Serial.print(thisMagnetometerNo);
        Serial.println(" from 8G to 4G");
      }
*/
  }
}

void handleUICommands(int maxSize)
{
  isHandlingUI=true;

  // simple handshake used to see whether the uP is processing commands
  if(matchesCommand(incomingCommand,commandText[HANDSHAKE],strlen(commandText[HANDSHAKE])))
  {
    Serial.println("A,");
  }

  // recalibrate the magnetometers
  if(matchesCommand(incomingCommand,commandText[RECALIBRATE],maxSize))
  {
    // do this inline for now
    digitalWrite(YELLOW_LED, true);           
    Serial.println("*** Recalibrate Sensors ***");
    calibrateMagnetometers();
    digitalWrite(YELLOW_LED, false);           
  }

  // set a new ema period/alpha
  if(matchesCommand(incomingCommand,commandText[EMA_PERIOD],strlen(commandText[EMA_PERIOD])))
  {
    Serial.println("*** Calculate New Filter Factors ***");

    // set the weighting factor for all results
    timeConstantInFrames=atoi(incomingCommand.c_str()+strlen(commandText[EMA_PERIOD]));
    Serial.print("New time constant (in frames) ");
    Serial.println(timeConstantInFrames);

    // note the rough sample time
    Serial.print("Sample speed (us) ");
    Serial.println(sampleTimeInUS);
    
    // calc time constant in seconds bearing in mind the target frame rate
    float thisTimeConstantInSeconds=timeConstantInFrames/float(TARGET_FRAME_RATE);
    Serial.print("Time constant (ms) ");
    Serial.println(thisTimeConstantInSeconds*1000.0f);

    // we need to sendover both the desired time constant and the current sampling speed
    Result::SetFilterTimeConstant(timeConstantInFrames,thisTimeConstantInSeconds,sampleTimeInUS*0.000001f);

    Serial.print("Alpha ");
    Serial.println(Result::Alpha());

    Serial.print("No of samples to average ");
    Serial.println(Result::NoOfSamplesToAverage());
  }

  // choose which stat we want - mean or standard or filter EMAs
  if(matchesCommand(incomingCommand,commandText[STATISTIC],strlen(commandText[STATISTIC])))
  {
    Serial.println("*** Select New Smoothing Statistic ***");

    // set the weighting factor for all results
    int thisStatisticIndex=atoi(incomingCommand.c_str()+strlen(commandText[STATISTIC]));
    Serial.print("New statistic ");
    Serial.print(thisStatisticIndex);
    Serial.print(" / ");
    Serial.println(statisticName[thisStatisticIndex]);
    chosenStatistic=thisStatisticIndex;
  }
  
  if(matchesCommand(incomingCommand,commandText[CUTOFF],strlen(commandText[CUTOFF])))
  {
    Serial.println("*** Select New Cutoff Frequency ***");

    // set the weighting factor for all results
    float thisCutoffFrequency=atof(incomingCommand.c_str()+strlen(commandText[CUTOFF])+1);
    Serial.print("New cutoff frequency ");
    Serial.println(thisCutoffFrequency);
    Result::SetCutoffFrequency(thisCutoffFrequency);
  }

  if(matchesCommand(incomingCommand,commandText[BETA],strlen(commandText[BETA])))
  {
    Serial.println("*** Select New Beta ***");

    // set the weighting factor for all results
    float thisBeta=atof(incomingCommand.c_str()+strlen(commandText[BETA])+1);
    Serial.print("New beta ");
    Serial.println(thisBeta);
    Result::SetBeta(thisBeta);
  }

  if(matchesCommand(incomingCommand,commandText[XYZ_READING],strlen(commandText[XYZ_READING])))
  {
    Serial.println("*** Read single X,Y,Z reading ***");

    Serial.println("#,Name,X,Y,Z,Temperature (C)");
    for(int idx=0;idx<NO_OF_MAGNETOMETERS;++idx)
    {
      Magnetometer* thisMagnetometer=readMagnetometer(idx,true);
      float temperature=thisMagnetometer->getTemperatureInCelcius();

      Serial.print(idx);
      Serial.print(",");
      Serial.print(magnetometerName[idx]);
      Serial.print(",");
      Serial.print(thisMagnetometer->m.x);
      Serial.print(",");
      Serial.print(thisMagnetometer->m.y);
      Serial.print(",");
      Serial.print(thisMagnetometer->m.z);
      Serial.print(",");
      Serial.println(temperature);
    }
  }

  isHandlingUI=false;
}

bool isDataAvailable(int idx)
{
  // the data ready interrupts are not reliable so read the lines directly
  // see page 13 of AN4602
  int thisPin=magnetometerToInterruptPin[idx];
  bool isReady=digitalRead(thisPin);
  return(isReady);
}

void statisticsTest()
{
  static Result statTest;
  static bool hasSetFilter=false;
  static float thisValue=1000.0f;
  
  if(!hasSetFilter)
  {
    Result::SetFilterTimeConstant(1,3.0f/30.0f,0.01);
    Serial.println("Alpha");  
    Serial.println(statTest.Alpha());
    statTest.AddResult(thisValue);
    hasSetFilter=true;
  }

  // get a delta fraction in the range +-5%
  float thisDelta=random(-500,+500)/10000.0f;
  if(thisDelta>0)
    thisValue*=1+thisDelta;
  else
    thisValue/=1-thisDelta;
  
  statTest.AddResult(thisValue);
  
  Serial.println("Delta,Count,MA,EMA,Double EMA,One Euro");

  Serial.println(thisDelta);
  Serial.println(statTest.Count());
  Serial.println(statTest.MovingAverage());
  Serial.println(statTest.EMA());
  Serial.println(statTest.EMADouble());
  Serial.println(statTest.OneEuro());

  delay(10);
  return;
}

void loop()
{ 
  // statisticsTest();
  // return;
  
  static unsigned long lastSecond=millis()/1000;
      
  // ignore all app commands if we're in the middle of calibration etc
  if(isHandlingUI)
  {
    Serial.println("U,");
    return;
  }
   
  if(USE_TIMING_PINS)
    // one blip per 5-sensor scan
    digitalWrite(SENSOR_SCAN_PIN, false);  
 
  // watch the data ready flags and retrieve the data from the chips
  for(int idx=0;idx<NO_OF_MAGNETOMETERS;++idx) 
  {   
    if(true /*isDataAvailable(idx)*/)
    {
      if(USE_TIMING_PINS)
        // one blip per individual sensor scan
        digitalWrite(SENSOR_READ_PIN, false);  
      
      Magnetometer* thisMagnetometer=readMagnetometer(idx);

      if(USE_TIMING_PINS)
        // one blip per individual sensor scan
        digitalWrite(SENSOR_READ_PIN, true);  

      resultVector[idx].x.AddResult(thisMagnetometer->m.x);
      resultVector[idx].y.AddResult(thisMagnetometer->m.y);
      resultVector[idx].z.AddResult(thisMagnetometer->m.z);

/*
      if(!idx && resultVector[0].x.Count()(us)==2)
      {
        Serial.print("Sample time (us)");
        Serial.println(resultVector[0].x.SampleTime());
        Serial.println(resultVector[0].x.SampleSpeedInUS());
      }
*/

      if(SEND_DETAILED_DATA)
      {
        Serial.print("I,");
        Serial.print(thisMagnetometer->m.x);
        Serial.print(",");
        Serial.print(thisMagnetometer->m.y);
        Serial.print(",");
        Serial.println(thisMagnetometer->m.z);
      }
      
      // calculate overall noise number based on the value of the sensor readings
      noiseCalc.AddResult(thisMagnetometer->m.x);
      noiseCalc.AddResult(thisMagnetometer->m.y);
      noiseCalc.AddResult(thisMagnetometer->m.z);      
    }
  }
  
  if(SHOULD_CHANGE_SCALE)
    changeMagnetometerScaling();
 
  if(USE_TIMING_PINS)
    // one blip per 5-sensor scan - length shows scan time
    digitalWrite(SENSOR_SCAN_PIN, true);  

  // expect one prompt every frame
  if(Serial.available())
  {
    int maxSize=getIncomingCommandFromApp();
    
    // send out data for the new frame
    if(matchesCommand(incomingCommand,commandText[NEW_FRAME],maxSize))
    {
      // report the overall noise number every second
      // this mainly makes sense when the magnet is not near the sensors and we're getting their idle noise
      // do this before we zap the stats
      unsigned long thisSecond=millis()/1000;
      if(SEND_STATS && thisSecond>lastSecond)
      {
        static bool hasOutputHeadings=false;
        if(!hasOutputHeadings)
        {
          Serial.println("H,Count,Time(S),Sample Time(us),Statistic,TimeConstantInFrames,Alpha(%),Cutoff,Beta,NoOfSamplesToAverage,Mean,SD,SNR,SNR(dB),SEM,RSEM");
          hasOutputHeadings=true;
        }
       
        // get rough sample time - any sensor will do as they're all running at about the same speed
        // note that we're getting this at the end of the frame
        sampleTimeInUS=resultVector[0].x.SampleSpeedInUS();
    
        Serial.print("N,");
        Serial.print(noiseCalc.Count());
        Serial.print(",");
        Serial.print(thisSecond);
        Serial.print(",");
        Serial.print(sampleTimeInUS);
        Serial.print(",");

        Serial.print(statisticName[chosenStatistic]);
        Serial.print(",");
        Serial.print(timeConstantInFrames);
        Serial.print(",");
        
        float alphaAsPercentage=Result::Alpha()*100.0f;
        Serial.print(alphaAsPercentage);
        Serial.print(",");
        Serial.print(Result::CutoffFrequency());
        Serial.print(",");
        Serial.print(Result::Beta());
        Serial.print(",");
        Serial.print(Result::NoOfSamplesToAverage());
        Serial.print(",");
        Serial.print(noiseCalc.Mean());
        Serial.print(",");
        Serial.print(noiseCalc.StandardDeviation());
        Serial.print(",");
        Serial.print(noiseCalc.SNR());
        Serial.print(",");
        Serial.print(noiseCalc.SNRdB());
        Serial.print(",");
        Serial.print(noiseCalc.StandardErrorOfMean());
        Serial.print(",");
        // in percent
        Serial.println(noiseCalc.RelativeStandardErrorOfMean()*100.0f);
        noiseCalc.Zap();
        lastSecond=thisSecond;
      }
      
      if(USE_TIMING_PINS)
        // one blip per frame
        digitalWrite(NEW_FRAME_PIN, false);
        
      // send results and reset the averages
      if(SEND_SERIAL_DATA_IN_BINARY)
        outputResultsAsBinary();
      else
        // text 
        outputResultsAsText();
      
      if(USE_TIMING_PINS)
        // one blip per frame - length shows comms time
        digitalWrite(NEW_FRAME_PIN, true);  
    }

    // process the rest of the app commands
    handleUICommands(maxSize);
  }
}

// Text serial output methods

void printVector(int thisMagnetometerNo,Magnetometer::vector<Result>& thisResult,bool shouldAddEndOfLine)
{ 
  switch(chosenStatistic)
  {
    case MEAN:
      Serial.print(thisResult.x.Mean());
      Serial.print(",");
      Serial.print(thisResult.y.Mean());
      Serial.print(",");
      Serial.print(thisResult.z.Mean());
      break;

    case MOVING_AVERAGE:
      Serial.print(thisResult.x.MovingAverage());
      Serial.print(",");
      Serial.print(thisResult.y.MovingAverage());
      Serial.print(",");
      Serial.print(thisResult.z.MovingAverage());
      break;

    case EMA:
      Serial.print(thisResult.x.EMA());
      Serial.print(",");
      Serial.print(thisResult.y.EMA());
      Serial.print(",");
      Serial.print(thisResult.z.EMA());
      break;         

    case EMA_DOUBLE:
      Serial.print(thisResult.x.EMADouble());
      Serial.print(",");
      Serial.print(thisResult.y.EMADouble());
      Serial.print(",");
      Serial.print(thisResult.z.EMADouble());
      break;         

    case ONE_EURO:
      Serial.print(thisResult.x.OneEuro());
      Serial.print(",");
      Serial.print(thisResult.y.OneEuro());
      Serial.print(",");
      Serial.print(thisResult.z.OneEuro());
      break;         
  }
  
  Serial.print(",");
  // the counts and timestamps for each dimension should be the same
  Serial.print((int)(thisResult.x.EMACount()));
  Serial.print(",");
  Serial.print((int)(thisResult.x.TimeStamp()));
  Serial.print(",");
  // also supply sample count and peak-to-peak noise as an easily calculated noise value
  Serial.print(thisResult.x.StandardDeviation());
  Serial.print(",");
  Serial.print(thisResult.y.StandardDeviation());
  Serial.print(",");
  Serial.print(thisResult.z.StandardDeviation());
  Serial.print(",");
  // tack on the current scale factor for debugging
  Serial.print(magnetometers[thisMagnetometerNo]->currentScale);
  
  // handle the end of line
  if(shouldAddEndOfLine)
    Serial.println();
  else
    Serial.print(",");
}

void outputResultsAsText()
{
  // always start with 'D,' so other serial outputs can be ignored quickly
  Serial.print("D,");

  Serial.print(sizeof(serialOutputData));
  Serial.print(",");

  Serial.print(NO_OF_MAGNETOMETERS);
  Serial.print(",");
   
  for(int idx=0;idx<NO_OF_MAGNETOMETERS;++idx)
  {
    printVector(idx,resultVector[idx],idx==NO_OF_MAGNETOMETERS-1);
    
    // zap the current results
    ZapResultVector(resultVector[idx]);
  }
}

// Binary serial output methods

int16_t castToShort(float thisValue)
{
  return(int16_t(thisValue));
}

int16_t castToShortAndScale(float thisValue)
{
  return((int16_t(thisValue))>>2);
}

void marshalResultVector(SensorReading& thisSensorOutput,Magnetometer::vector<Result>& thisResult,int thisMagnetometerNo)
{ 
  // shift and cast the sensor values to get back to 16 bits so we can xmit the smallest data packet
  // the loss in accuracy is inconsequential as the _max_ accuracy we get is about 10 bits
  switch(chosenStatistic)
  {
    case MEAN:
      thisSensorOutput.x=thisResult.x.Mean();
      thisSensorOutput.y=thisResult.y.Mean();
      thisSensorOutput.z=thisResult.z.Mean();
      break;

    case MOVING_AVERAGE:
      thisSensorOutput.x=thisResult.x.MovingAverage();
      thisSensorOutput.y=thisResult.y.MovingAverage();
      thisSensorOutput.z=thisResult.z.MovingAverage();
      break;

    case EMA:
      thisSensorOutput.x=thisResult.x.EMA();
      thisSensorOutput.y=thisResult.y.EMA();
      thisSensorOutput.z=thisResult.z.EMA();
      break;         

    case EMA_DOUBLE:
      thisSensorOutput.x=thisResult.x.EMADouble();
      thisSensorOutput.y=thisResult.y.EMADouble();
      thisSensorOutput.z=thisResult.z.EMADouble();
      break;         

    case ONE_EURO:
      thisSensorOutput.x=thisResult.x.OneEuro();
      thisSensorOutput.y=thisResult.y.OneEuro();
      thisSensorOutput.z=thisResult.z.OneEuro();
      break;         
  }

  thisSensorOutput.xNoise=thisResult.x.StandardErrorOfMean();
  thisSensorOutput.yNoise=thisResult.y.StandardErrorOfMean();
  thisSensorOutput.zNoise=thisResult.z.StandardErrorOfMean();
  
  thisSensorOutput.scaleFactor=magnetometers[thisMagnetometerNo]->currentScale;
}

void outputResultsAsBinary()
{
  // always start with 'D,' so other serial outputs can be ignored quickly
  serialOutputData.d='D';
  serialOutputData.comma=',';

  // struct length for checking by receiving app
  serialOutputData.byteCount=sizeof(serialOutputData);
  serialOutputData.sensorCount=NO_OF_MAGNETOMETERS;

  // only send one value of sample count and time stamp, rather than 15 values of each, as they'll all be very similar
  serialOutputData.sampleCount=resultVector[0].x.EMACount();
  serialOutputData.timeStamp=resultVector[0].x.TimeStamp();

  /* the magnetometer result stats will probbaly always contain more information than
     we want to squeeze down for transmission, so this xfer will probably always be necessary. */
  for(int idx=0;idx<NO_OF_MAGNETOMETERS;++idx)
  {
    marshalResultVector(serialOutputData.readings[idx],resultVector[idx],idx);
    
    // zap the current results
    ZapResultVector(resultVector[idx]);
  }

  // use CR LF as an extra end-of-buffer indicator
  serialOutputData.cr=CR;
  serialOutputData.lf=LF;   

  // finally squirt the buffer at the serial port
  Serial.write(serialOutputBuffer,sizeof(serialOutputData));
}
