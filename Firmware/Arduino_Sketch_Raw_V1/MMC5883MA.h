#ifndef MMC5883MA_h
#define MMC5883MA_h

#include <Arduino.h>
#include ".\Magnetometer.h"

static const int XscaleFactor[]={4,8,12,16};

class MMC5883MA:public Magnetometer
{
public:
    // register addresses
    enum registers {X_L_REG=0x00, X_H_REG, Y_L_REG, Y_H_REG, Z_L_REG, Z_H_REG,
                    TEMPERATURE_REG=0x06,STATUS_REG,CONTROL_0_REG,CONTROL_1_REG,CONTROL_2_REG,
                    X_THRESHOLD_REG,Y_THRESHOLD_REG,Z_THRESHOLD_REG,
                    PRODUCT_ID_REG=0x2F};

    // status reg
    enum statusRegister {FIELD_MEASUREMENT_DONE=0x01, TEMPERATURE_MEASUREMENT_DONE=0x02,
                         MOTION_DETECTED=0x04, CHARGE_PUMP_ON=0x08, CONFIG_DATA_READ=0x10};

    // Control Reg 0 - initiate measurements, motion detection, set & reset, calibration data
    enum control0 {INITIATE_FIELD_MEASUREMENT=0x01, INITIATE_TEMPERATURE_MEASUREMENT=0x02,
                   START_MOTION_DETECT=0x04, INITIATE_SET_PULSE=0x08, INITIATE_RESET_PULSE=0x10,
                   READ_CONFIG_DATA=0x40};
                   
    // Control Reg 1 - bandwidth & reset
    enum control1 {ODR_100Hz=0x00, ODR_200Hz=0x01, ODR_400Hz=0x02, ODR_600Hz=0x03, SW_RST=0x80};

    // Control Reg 2 - continuous measurement rate, motion detection, measurement done
    enum control2 {CM_OFF=0x00, CM_14Hz, CM_5Hz, CM_2_2Hz, CM_1Hz, /* etc */
                   MOTION_DETECTED_INT=0x20, MEASUREMENTS_COMPLETED_INT=0x40};   

    enum timeout {POWER_UP_TIME_IN_MS=5,SET_RESET_PULSE_WAIT_TIME_IN_MS=1,
                  MISC_IO_TIMEOUT=0,SELF_TEST_TIMEOUT_IN_MS=0,
                  DATA_READ_TIME_IN_MS=10,TEMPERATURE_READ_TIME_IN_MS=3};

    // the MMC5883MA has fixed +-8G scaling
    enum misc {I2C_ADDRESS=0x30,PRODUCT_ID=0x0C,SCALING=8,SCALING_FACTOR_16G=2};
    
    MMC5883MA() {address=I2C_ADDRESS; offsetVector.x=0; offsetVector.y=0; offsetVector.z=0;};
      
    virtual bool init(uint8_t thisAddress=0, bool shouldLog=false);
    virtual void resetDevice();
    virtual bool selfTest(bool shouldLogResult=false) {return(true);};
    virtual void setDataRate();
    virtual bool readXYZ(bool shouldLog=false);
    virtual bool setOffset(int32_t x,int32_t y,int32_t z)
      // note that we add the vectors as we may calibrate more than once
      {offsetVector.x+=x; offsetVector.y+=y; offsetVector.z+=z; return(true);};
    virtual bool checkDataReady(uint8_t thisStatus, bool shouldLog=false);
    virtual bool waitForDataReady(uint16_t timeOutInUS,bool shouldLog=false);
    bool checkTemperatureReady(uint8_t thisStatus, bool shouldLog=false);
    bool waitForTemperatureReady(uint16_t timeOutInUS,bool shouldLog=false);
    virtual uint16_t getSelfTestTimeoutInUS() {return(DATA_READ_TIME_IN_MS*1000);};
    virtual float getTemperatureInCelcius();

protected:
    // used to null out readings after calibration
    vector<int32_t> offsetVector;
};

#endif
