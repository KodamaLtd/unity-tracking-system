#ifndef AK09940_h
#define AK09940_h

#include <Arduino.h>
#include ".\Magnetometer.h"

class AK09940:public Magnetometer
{
public:
    // register addresses
    enum registers {WIA1=0x00, WIA2=0x01, ST1=0x10,
                    HXL=0x11, HXH, HYL, HYH, HZL, HZH,
                    TMPS=0x1A, ST2=0x1B,
                    CNTL1=0x30,CNTL2,CNTL3,CNTL4,
                    I2CDIS=0x36, TS=0x37};

    // status reg (both ST1 & ST2)
    enum statusRegister {DATA_READY=0x01, FIFO_STATUS=0x1E, INV=0x02, DOR=0x01};

    // Control Reg 1 - watermark
    enum control1 {WATERMARK=0x07};

    // Control Reg 2 - temperature
    enum control2 {ENABLE_TEMPERATURE_MEASUREMENT=0x40};   

    // Control Reg 3 - mode, enable/disable FIFO, sensor drive
    /* power/noise options:
     *  
     *  Low Power 0 - 0.03mA / 70nT
     *  Low Power 1 - 0.06mA / 60nT
     *  Low Noise 0 - 0.1mA  / 50nT
     *  Low Noise 1 - 0.2mA /  40nT
     */
    enum control3 {ENABLE_FIFO=0x80, SENSOR_DRIVE=0x60, MODE=0x1F,
                   POWER_DOWN_MODE=0x00, SINGLE_MEASUREMENT_MODE=0x01, SELF_TEST_MODE=0x10,
                   CM_10Hz=0x02,CM_20Hz=0x04,CM_50Hz=0x06,CM_100Hz=0x08,CM_200Hz=0x0A,CM_400Hz=0x0C,
                   LOW_POWER_DRIVE_0=0x00,LOW_POWER_DRIVE_1=0x20,LOW_NOISE_DRIVE_0=0x40,LOW_NOISE_DRIVE_1=0x60};   

    // Control Reg 4 - soft reset
    enum control4 {SOFT_RESET=0x01};   

    // timeouts
    enum timeout {POWER_UP_TIME_IN_MS=50,MODE_WAIT_TIME_IN_US=100,RESET_TIME_IN_US};

    // the AK09940 has fixed +-12G scaling
    enum misc {I2C_ADDRESS=0x0C,COMPANY_ID=0x48,DEVICE_ID=0xA1,SENSOR_OVERFLOW=0x1FFFF};
    
    AK09940() {address=I2C_ADDRESS; offsetVector.x=0; offsetVector.y=0; offsetVector.z=0;};
      
    virtual bool init(uint8_t thisAddress=0, bool shouldLog=false);
    virtual void resetDevice();
    virtual bool selfTest(bool shouldLogResult=false) {return(true);};
    virtual void setDataRate();
    virtual bool readXYZ(bool shouldLog=false);
    virtual bool setOffset(int32_t x,int32_t y,int32_t z)
      // note that we add the vectors as we may calibrate more than once
      {offsetVector.x+=x; offsetVector.y+=y; offsetVector.z+=z; return(true);};
    virtual bool checkDataReady(uint8_t thisStatus, bool shouldLog=false);
    virtual bool waitForDataReady(uint16_t timeOutInUS,bool shouldLog=false);
    bool checkTemperatureReady(uint8_t thisStatus, bool shouldLog=false);
    bool waitForTemperatureReady(uint16_t timeOutInUS,bool shouldLog=false);
    virtual uint16_t getSelfTestTimeoutInUS() {return(0*1000);};
    virtual float getTemperatureInCelcius();

protected:
    // used to null out readings after calibration
    vector<int32_t> offsetVector;
};

#endif
