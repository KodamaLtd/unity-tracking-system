
#include "DebugUART.h"


#ifdef ARDUINO_SERIAL
#define UART_PORT ARDUINO_SERIAL
#endif

#ifdef ARDUINO_SERIAL1
#define UART_PORT ARDUINO_SERIAL1
#endif

#ifdef ARDUINO_SERIAL2
#define UART_PORT ARDUINO_SERIAL2
#endif

#ifdef ARDUINO_SERIAL3
#define UART_PORT ARDUINO_SERIAL3
#endif

void DebugUART::begin(uint32_t baud)
{
	// disable UART tx int
#ifdef ARDUINO_SERIAL
	Serial.begin(baud);
	UART_PORT->UART_IDR = UART_IDR_TXRDY;
#endif
#ifdef ARDUINO_SERIAL1
	Serial1.begin(baud);
	UART_PORT->US_IDR = US_IDR_TXRDY;
#endif
#ifdef ARDUINO_SERIAL2
	Serial2.begin(baud);
	UART_PORT->US_IDR = US_IDR_TXRDY;
#endif
#ifdef ARDUINO_SERIAL3
	Serial3.begin(baud);
	UART_PORT->US_IDR = US_IDR_TXRDY;
#endif
}

void DebugUART::PutChar(char c)
{
#ifdef ARDUINO_SERIAL
	while ((UART_PORT->UART_SR & UART_SR_TXRDY) != UART_SR_TXRDY)
		;
	UART_PORT->UART_THR = c;
#else
	while ((UART_PORT->US_CSR & US_CSR_TXRDY) != US_CSR_TXRDY)
		;
	UART_PORT->US_THR = c;
#endif
}

void DebugUART::print(const char *s)
{
	while (*s)
		PutChar(*(s++));
}

void DebugUART::print(const unsigned long no)
{
  char buffer[16];
  snprintf(buffer,sizeof(buffer),"%lu",no);
  print(buffer);
}

void DebugUART::print(const long no)
{
  char buffer[16];
  snprintf(buffer,sizeof(buffer),"%l",no);
  print(buffer);
}

void DebugUART::print(const unsigned int no)
{
  char buffer[16];
  snprintf(buffer,sizeof(buffer),"%u",no);
  print(buffer);
}

void DebugUART::print(const int no)
{
  char buffer[16];
  snprintf(buffer,sizeof(buffer),"%i",no);
  print(buffer);
}
