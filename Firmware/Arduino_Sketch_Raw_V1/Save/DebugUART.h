
#ifndef DEBUGUART_H
#define DEBUGUART_H

#include <Arduino.h>

// Un-comment ONE of the following lines, to select the UART to use for debug
//#define ARDUINO_SERIAL	((Uart *)UART)
//#define ARDUINO_SERIAL1	((Usart *)USART0)
//#define ARDUINO_SERIAL2		((Usart *)USART1)
//#define ARDUINO_SERIAL3	((Usart *)USART3)


class DebugUART
{
public:
	DebugUART() {}
	~DebugUART() {}
	void begin(uint32_t baud);
	void PutChar(char c);
	void print(const char *s);
  void println(const char *s) {print(s); print("\n");};
  void flush() {/* do nothing */};

  void print(const unsigned long no);
  void println(const unsigned long no) {print(no); print("\n");};
  void print(const long no);
  void println(const long no) {print(no); print("\n");};
  void print(const unsigned int no);
  void println(const unsigned int no) {print(no); print("\n");};
  void print(const int no);
  void println(const int no) {print(no); print("\n");};
};

#endif
