#ifndef Magnetometer_h
#define Magnetometer_h

#include <Arduino.h>

class Magnetometer
{
  public:
    template <typename T> struct vector
    {
      T x, y, z;
    };

    vector<int32_t> m; // magnetometer readings
    uint8_t last_status; // status of last I2C transmission
    int currentScale=0;

    // vector functions
    template <typename Ta, typename Tb, typename To> static void vector_cross(const vector<Ta> *a, const vector<Tb> *b, vector<To> *out);
    template <typename Ta, typename Tb> static float vector_dot(const vector<Ta> *a, const vector<Tb> *b);
    static void vector_normalize(vector<float> *a);

    Magnetometer() {};
    
    virtual bool init(uint8_t thisAddress=0, bool shouldLog=false)=0;
    virtual void resetDevice()=0;
    virtual bool selfTest(bool shouldLogResult=false)=0;
    virtual void setDataRate()=0;
    virtual bool readXYZ(bool shouldLog=false)=0;    
    virtual bool calculateAverageOfNReadings(uint8_t noOfSamples,vector<int32_t>& average, bool shouldLogResult=false);
    virtual bool setOffset(vector<int32_t>& offset) {return(setOffset(offset.x,offset.y,offset.z));};
    virtual bool setOffset(int32_t x,int32_t y,int32_t z)=0;
    virtual bool checkDataReady(uint8_t thisStatus, bool shouldLog=false)=0;
    virtual bool waitForDataReady(uint16_t timeOutInUS,bool shouldLog=false)=0;
    virtual uint16_t getSelfTestTimeoutInUS()=0;
    virtual bool setScale(int newScale) {};
    virtual float getTemperatureInCelcius() {return(0);};

    void writeReg(uint8_t reg, uint8_t value);
    uint8_t readReg(uint8_t reg);
    bool testRegExists(uint8_t thisRegister);

  protected:
    uint8_t address;  
};

template <typename Ta, typename Tb, typename To> void Magnetometer::vector_cross(const vector<Ta> *a, const vector<Tb> *b, vector<To> *out)
{
  out->x = (a->y * b->z) - (a->z * b->y);
  out->y = (a->z * b->x) - (a->x * b->z);
  out->z = (a->x * b->y) - (a->y * b->x);
}

template <typename Ta, typename Tb> float Magnetometer::vector_dot(const vector<Ta> *a, const vector<Tb> *b)
{
  return (a->x * b->x) + (a->y * b->y) + (a->z * b->z);
}

#endif
