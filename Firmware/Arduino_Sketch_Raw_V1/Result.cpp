
#include "Result.h"

float Result::filterTimeConstant=Result::SetFilterTimeConstant(1,DEFAULT_FILTER_TIME_CONSTANT_IN_MS/1000.0f,DEFAULT_FILTER_TIME_CONSTANT_IN_MS/1000.0f);
float Result::emaAlpha;
int Result::noOfSamplesToAverage;
float Result::mincutoff;
float Result::beta;
float Result::dcutoff;
  
float Result::SetFilterTimeConstant(int timeConstantInFrames,float thisFilterTimeConstant,float thisSampleTime)
{
  // the time constant is expected in seconds
  filterTimeConstant=thisFilterTimeConstant;
  
  // calculate the factors for the various filters - all based on the time constant
  emaAlpha=1-exp(- thisSampleTime / filterTimeConstant);

  noOfSamplesToAverage=(int)round(filterTimeConstant/thisSampleTime);
  if(noOfSamplesToAverage<1)
    noOfSamplesToAverage=1;

  // return the calling parameter for the convenience of the static initialisation
  return(filterTimeConstant);
}

void Result::AddResult(float value)
{
  if(!isfinite(value))
    return;

  //  this is platform dependent
  timeStamp=micros();

  if(!actualCount)
  {
    startTimeStamp=timeStamp;
    lastTimeStamp=timeStamp;
    sampleTime=0;
  }
  else
  {  
    sampleTime=timeStamp-lastTimeStamp;
    previousTimeStamp=lastTimeStamp;
    unsigned long tempTimeStamp=timeStamp;
    lastTimeStamp=tempTimeStamp; 
  }
  ++actualCount;

  // standard population stats
  sum += value;
  sumOfSquares += value * value;

  float thisMean = Mean();
  meanSum += thisMean;
  meanSumOfSquares += thisMean * thisMean;

#if !defined(MAX_PERF)   
  float thisSD = StandardDeviation();
  sdSum += thisSD;
  sdSumOfSquares += thisSD * thisSD;
#endif

  // peak-to-peak
  if (actualCount == 1)
  {
    maxPeak = value;
    minPeak = value;
  }
  else
  {
    if (value > maxPeak)
      maxPeak = value;

    if (value < minPeak)
      minPeak = value;
  }

  // moving average
  if(actualCount<=noOfSamplesToAverage)
    movingAverage=Mean();
  else
    movingAverage=lastMovingAverage+(value-lastMovingAverage)/noOfSamplesToAverage;
      
  lastMovingAverage=movingAverage;

  // EMA
  ++emaCount; 
  
  // EMA filter
  if(!actualCount)
    ema=value;
  else
    ema=lastEma*(1-emaAlpha)+value*emaAlpha;   
    
  lastEma=ema;

  // double EMA filter
  if(!actualCount)
    emaOfEma=ema;
  else
    emaOfEma=lastEmaOfEma*(1-emaAlpha)+ema*emaAlpha;   
    
  lastEmaOfEma=emaOfEma;

  emaDouble=2*ema-emaOfEma;
  lastEmaDouble=emaDouble;

  // one euro
  float rate;
  if(sampleTime)
    // sample time is in uS
    rate=1000000.0f/sampleTime;
  else
    // assume something - the initial effect will soon vanish
    // a typical sample time here is ~250ms, giving a rate of ~4
    rate=1000.0f/250.0f;
   
  float dx;
  if(oneEuroFirstTime)
  {
    dx=0;
    oneEuroFirstTime=false;
  }
  else
    dx=(value-xfilt.hatxprev)*rate;

  float edx=dxfilt.Filter(dx,Alpha(rate,dcutoff));
  float cutoff=mincutoff+beta*abs(edx);
  oneEuro=xfilt.Filter(value,Alpha(rate,cutoff));

  // oneEuroDebug(value,rate,dx,edx,cutoff);
}

void Result::oneEuroDebug(float value,float rate, float dx, float edx, float cutoff)
{
  Serial.print("Sensor No ");
  Serial.print(magnetometerNo);
  Serial.print(", Dimension ");
  Serial.print(dimension);
  Serial.print(", Timestamp ");
  Serial.print(timeStamp);
  Serial.print(", Last Timestamp ");
  Serial.print(previousTimeStamp);
  Serial.print(", Result ");
  Serial.print(value);
  Serial.print(", Sample Time ");
  Serial.print(sampleTime);
  Serial.print(", Rate ");
  Serial.print(rate);
  Serial.print(", dx ");
  Serial.print(dx);
/*  
  Serial.print(", dcutoff ");
  Serial.print(dcutoff);
  Serial.print(", mincutoff ");
  Serial.print(mincutoff);
*/
  Serial.print(", edx ");
  Serial.print(edx);
  Serial.print(", cutoff ");
  Serial.print(cutoff);
  Serial.print(", Euro ");
  Serial.println(oneEuro);
}

float FilterClass::Filter(float x, float alpha)
{
  if(firstTime)
  {
    hatxprev=x;
    firstTime=false;
  }

  float hatx=alpha*x+(1-alpha)*hatxprev;
  hatxprev=hatx;
  return(hatx);
}

float Result::Alpha(float rate, float cutoff)
{
  float tau=1/(2*PI*cutoff);
  float te=1/rate; 
  return (1/(1+tau/te));
}

float Result::Mean()
{
  return (sum / actualCount);
}

float Result::PeakToPeak()
{
  return (maxPeak-minPeak);
}

#if !defined(MAX_PERF)   

float Result::StandardDeviation()
{
  if (actualCount == 0)
    return (0);

  float mean = Mean();
  float standardDeviation = sqrt(sumOfSquares / actualCount - mean * mean);
  return (standardDeviation);
}

float Result::MeanSD()
{
  if (actualCount == 0)
    return (0);

  return (sdSum / actualCount);
}

float Result::SDSD()
{
  if (actualCount == 0)
    return (0);

  float mean = MeanSD();
  float standardDeviation = sqrt(sdSumOfSquares / actualCount - mean * mean);
  return (standardDeviation);
}

float Result::RSESD()
{
  // return (SDSD() / StandardDeviationInPPM());
  if (actualCount <= 1)
    return (0);

  return (1 / sqrt(2 * (actualCount - 1)));
}
#endif
