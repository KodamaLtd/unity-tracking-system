#include ".\MMC5883MA.h"
#include <Wire.h>
#include <math.h>

bool MMC5883MA::init(uint8_t thisAddress, bool shouldLog)
{
  // actually there's only one possible address, so set it to the default
  address=I2C_ADDRESS;

  // check that we can read the baked-in product ID
  uint8_t thisID=readReg(PRODUCT_ID_REG);
  bool isFound=thisID==PRODUCT_ID;
  
  if(shouldLog)
  {
    if(isFound)
    {
      Serial.print("Found product id ");
      Serial.print(thisID);
      Serial.print(" on address ");
      Serial.println(address);
    }
    else
    {
      Serial.println("Couldn't locate device"); 
      return(false);
    }

    float thisTemperature=getTemperatureInCelcius();
    Serial.print("Temperature is ");
    Serial.println(thisTemperature);
  }
  
  return(isFound);
}

float MMC5883MA::getTemperatureInCelcius()
{
  writeReg(CONTROL_0_REG, INITIATE_TEMPERATURE_MEASUREMENT);
  if(waitForTemperatureReady(TEMPERATURE_READ_TIME_IN_MS*1000))
  {
    uint8_t thisTemperatureValue=readReg(TEMPERATURE_REG);
    return(-75+0.7*thisTemperatureValue);
  }
  else
  {
    Serial.print("Failed to read temperature");
    return(0);   
  }
}

void MMC5883MA::resetDevice()
{
  // software reset
  writeReg(CONTROL_1_REG, SW_RST);
  // page 9 of the REV C datasheet 
  delay(POWER_UP_TIME_IN_MS);

  // do both a set and a reset pulse
  writeReg(CONTROL_0_REG, INITIATE_SET_PULSE);
  delay(SET_RESET_PULSE_WAIT_TIME_IN_MS);

  writeReg(CONTROL_0_REG, INITIATE_RESET_PULSE);
  delay(SET_RESET_PULSE_WAIT_TIME_IN_MS);  
}

void MMC5883MA::setDataRate()
{
  // set continuous measurement rate and get an interrupt when the reading is done
  writeReg(CONTROL_2_REG, CM_14Hz | MEASUREMENTS_COMPLETED_INT);
  
  // set to lowest noise / highest current / low output rate
  writeReg(CONTROL_1_REG, ODR_600Hz);

  // set continuous measurements going
  writeReg(CONTROL_0_REG, INITIATE_FIELD_MEASUREMENT);
}

bool MMC5883MA::checkDataReady(uint8_t thisStatus, bool shouldLog)
{
  // check given status
  if(shouldLog)
    Serial.println(thisStatus & FIELD_MEASUREMENT_DONE?"data ready":"no data ready");

  return(thisStatus & FIELD_MEASUREMENT_DONE);
}

bool MMC5883MA::checkTemperatureReady(uint8_t thisStatus, bool shouldLog)
{
  // check given status
  if(shouldLog)
    Serial.println(thisStatus & FIELD_MEASUREMENT_DONE?"data ready":"no data ready");

  return(thisStatus & TEMPERATURE_MEASUREMENT_DONE);
}

bool MMC5883MA::waitForDataReady(uint16_t timeOutInUS,bool shouldLog)
{
  // read and wait for status until ready (with timeout)
  unsigned long startTime=micros();
  unsigned long elapsedTime;
  bool isDataReady=false;
  
  do
  {
    uint8_t thisStatus=readReg(STATUS_REG);
    if(shouldLog)
    {
      Serial.print("status ");
      Serial.println(thisStatus);
    }
    isDataReady=checkDataReady(thisStatus,shouldLog);
    elapsedTime=micros()-startTime;
    if(shouldLog)
    {
      Serial.print("read time (us) ");
      Serial.println(elapsedTime);
    }
  }
  while(!isDataReady && elapsedTime<timeOutInUS);
  
  return(isDataReady); 
}

bool MMC5883MA::waitForTemperatureReady(uint16_t timeOutInUS,bool shouldLog)
{
  // read and wait for status until ready (with timeout)
  unsigned long startTime=micros();
  unsigned long elapsedTime;
  bool isDataReady=false;
  
  do
  {
    uint8_t thisStatus=readReg(STATUS_REG);
    if(shouldLog)
    {
      Serial.print("status ");
      Serial.println(thisStatus);
    }
    isDataReady=checkTemperatureReady(thisStatus,shouldLog);
    elapsedTime=micros()-startTime;
    if(shouldLog)
      Serial.println(elapsedTime);
  }
  while(!isDataReady && elapsedTime<timeOutInUS);
  
  return(isDataReady); 
}

bool MMC5883MA::readXYZ(bool shouldLog)
{
  // initiate a measurement
  unsigned long startTransactionTime=micros();
  writeReg(CONTROL_0_REG, INITIATE_FIELD_MEASUREMENT);
  
  if(!waitForDataReady(DATA_READ_TIME_IN_MS*1000))
  {
    Serial.print("Failed to read data");
    return(false);
  }

  Wire.beginTransmission(address);
  Wire.write(X_L_REG);
  Wire.endTransmission();
  
  Wire.requestFrom(address, (uint8_t)6);

  unsigned long startReceiveTime=millis();
  while (Wire.available() < 6)
  {
    if (MISC_IO_TIMEOUT>0 && (millis()-startReceiveTime)>MISC_IO_TIMEOUT)
      return(false);
  }

  uint8_t xlm = Wire.read();
  uint8_t xhm = Wire.read();
  uint8_t ylm = Wire.read();
  uint8_t yhm = Wire.read();
  uint8_t zlm = Wire.read();
  uint8_t zhm = Wire.read();

  unsigned long endTransactionTime=micros();
  if(shouldLog)
  {
    Serial.print("Data read time was ");
    Serial.print(endTransactionTime-startTransactionTime);
    Serial.println("uS");
  }
 
  // combine high and low bytes
  m.x = (uint32_t)(xhm << 8 | xlm)-32768;
  m.y = (uint32_t)(yhm << 8 | ylm)-32768;
  m.z = (uint32_t)(zhm << 8 | zlm)-32768;

  m.x-=offsetVector.x;
  m.y-=offsetVector.y;
  m.z-=offsetVector.z;
  
/*
  // Scale to 16G standard
  m.x*=SCALING_FACTOR_16G;
  m.y*=SCALING_FACTOR_16G;
  // our convention is that 'out-of-the-PCB' is +ve Z
  // so reverse the sign for the MMC5883MA
  m.z*=-SCALING_FACTOR_16G;     
*/    
  return(true);
}
